module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        exec: {
            prepare: {
                command: "cordova prepare",
                stdout: true,
                stderror: true
            },
            ngbuild: {
                command: "npm run dev:mobile",
                stdout: true,
                stdin: true,
                stderror: true
            }
        },
        watch: {
            files: ['src/**/*', 'build/**/*'],
            tasks: ['exec:ngbuild', 'exec:prepare', 'notify_hooks']
        },
        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 5, // maximum number of notifications from jshint output
                title: "(Led v2.0) Transpiler Notification", // defaults to the name in package.json, or will use project directory's name
                message: 'Code transpiled mother fucker :D',
                success: true, // whether successful grunt executions should be notified automatically
                duration: 3 // the duration of notification in seconds, for `notify-send only
            }
        }
    });

    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');
    grunt.registerTask('default', ['watch']);

};

import { LibroPage } from './app.po';

describe('libro App', () => {
  let page: LibroPage;

  beforeEach(() => {
    page = new LibroPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

import { LogProvider } from './providers/log.provider';
import { UserLoggedFieldsContract } from './contracts/user-logged-fields.contract';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router, RouterState, ActivatedRoute } from '@angular/router';
import { InputService } from './modules/services/input.service';
import { BookEvent } from './modules/events/book/book.event';
import { SystemUpdateService } from './modules/services/packager/system-update.service';
import { environment } from '../environments/environment';
import { Title } from '@angular/platform-browser';
import { PlatformContract } from './modules/services/contracts/platform/platform.contract';
import { DialogContract } from './modules/services/contracts/dialog/dialog.contract';
import { AuthGuardService } from './modules/services/guard/auth-guard.service';

declare var FastClick;
declare var StatusBar;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    private Log: LogProvider = LogProvider.getInstance('AppComponent');
    private state: RouterState;
    private activedRoute: ActivatedRoute;
    private child: any;
    @Input() isAuth: boolean = false;

    constructor(
        private title: Title,
        @Inject('PlatformService') private platform: PlatformContract,
        private router: Router,
        private bookEvent: BookEvent,
        private inputService: InputService,
        private systemUpdate: SystemUpdateService,
        @Inject('DialogService') private dialog: DialogContract,
        private auth: AuthGuardService
    ) {
        this.platform.ready().then(() => {

            this.state = this.router.routerState;
            this.activedRoute = this.state.root;
            this.child = this.activedRoute.firstChild;

            try {
                this.systemUpdate.check();
            } catch (error) {
                this.dialog.onAlert(error.message, 'SystemUpdateService: Falha na request http');
            }

            if (typeof StatusBar !== 'undefined') StatusBar.hide();

            this.platform.backbutton().then((event: Event) => {
                if (!event) return;

                event.preventDefault();
                return false;
            });

            window['handleOpenURL'] = data => {
                if (data) {
                    this.inputService.setData(data);

                    this.inputService
                        .filterData()
                        .then((book: any) => {
                            const currentRoute = this.router.url;
                            const dataToEmmit = this.inputService.toObject();
                            dataToEmmit['book_led_id'] = book.book_led_id;

                            const stringDataToEmit = JSON.stringify(dataToEmmit);
                            const targetRoute = (<string>'reader/') + book.book_led_id;

                            if (currentRoute.indexOf(targetRoute) !== -1) {
                                this.bookEvent.EchoExternalData.emit(stringDataToEmit);
                            } else {
                                this.router
                                    .navigateByUrl('reader/' + book.book_led_id)
                                    .then(() => {
                                        this.bookEvent.EchoExternalData.emit(stringDataToEmit);
                                    });
                            }
                        })
                        .catch(error => {
                            this.Log.save(error);
                        });
                }
            };

            document.addEventListener(
                'DOMContentLoaded',
                () => {
                    FastClick.attach(document.body);
                },
                false
            );

        });
    }

    ngOnInit() {
        this.title.setTitle(environment.app_name);
    }
}

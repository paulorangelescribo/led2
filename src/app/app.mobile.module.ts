import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AuthenticatorModule } from './modules/authenticator/authenticator.module';
import { AppRouterModule } from './modules/app-router/app.router.module';
import { ShelfModule } from './modules/shelf/shelf.module';
import { ReaderModule } from './modules/reader/reader.module';
import { EventsModule } from './modules/events/events.module';
import { GlobalComponentsModule } from './modules/global-components/global-components.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DaoModule } from './modules/daos/dao.module';
import { ServicesMobileModule } from './modules/services/mobile/services.mobile.module';
import { ServicesModule } from './modules/services/services.module';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        HttpClientModule,
        AppRouterModule,
        GlobalComponentsModule,
        EventsModule,
        DaoModule,
        ServicesMobileModule,
        ServicesModule
    ],
    providers: [
        Title
    ],
    bootstrap: [AppComponent]
})
export class AppMobileModule { }

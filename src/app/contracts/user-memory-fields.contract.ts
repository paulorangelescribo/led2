export interface UserMemoryFieldsContract {
    memory: Array<{key: string, value: any}>;
}

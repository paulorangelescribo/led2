import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../services/guard/auth-guard.service';
import { SelectivePreloadingStrategy } from '../../selective-preloading-strategy';

const appRoutes: Routes = [
    { path: 'index.html', redirectTo: '/shelf', pathMatch: 'full' },
    { path: '', redirectTo: '/shelf', pathMatch: 'full' },
    {
        path: 'login',
        loadChildren: 'app/modules/authenticator/authenticator.module#AuthenticatorModule',
        data: { preload: true }
    },
    {
        path: 'shelf',
        loadChildren: 'app/modules/shelf/shelf.module#ShelfModule',
        canLoad: [AuthGuardService]
    },
    {
        path: 'reader',
        loadChildren: 'app/modules/reader/reader.module#ReaderModule',
        canLoad: [AuthGuardService]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: SelectivePreloadingStrategy })],
    exports: [RouterModule],
    providers: [SelectivePreloadingStrategy]
})
export class AppRouterModule {}

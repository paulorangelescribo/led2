import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { AuthenticatorRouterModule } from './authenticator.router.module';
import { GlobalComponentsModule } from '../global-components/global-components.module';

@NgModule({
    imports: [CommonModule, FormsModule, GlobalComponentsModule, AuthenticatorRouterModule],
    declarations: [
        LoginComponent
    ]
})

export class AuthenticatorModule { }

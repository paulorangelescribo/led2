import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { BookDao } from "./book.dao";

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    BookDao
  ]
})
export class DaoModule {}

import { Injectable, EventEmitter } from '@angular/core';
import { DownloadableObject } from '../services/content-manager/downloadable-object';
@Injectable()
export class AppEvent {
  EchoVisibleComponent          = new EventEmitter<{toolbar: boolean, navbar: boolean}>();
  EchoSearch                    = new EventEmitter<{search: string}>();
  EchoIntentActivity            = new EventEmitter<any>();
  EchoProgress                  = new EventEmitter<{  book_id: string, book_status: string, book_progress: number }>();
  EchoDownloadObjectFinish    = new EventEmitter<DownloadableObject>();
  EchoDownloadObjectStart       = new EventEmitter<DownloadableObject>();
  EchoAriaHidden = new EventEmitter<boolean>();
  EchoShowManagementSpace       = new EventEmitter<boolean>();
  EchoSpaceRequestForObject     = new EventEmitter<DownloadableObject>();
  EchoSettingsReady             = new EventEmitter<any>();
  EchoResetSpaceSettingsModal = new EventEmitter<boolean>();
}

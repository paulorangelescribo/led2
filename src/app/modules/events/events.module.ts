import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookEvent } from './book/book.event';
import { AppEvent } from './app.event';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    BookEvent,
    AppEvent
  ]
})
export class EventsModule { }

import { Component, OnInit, Input } from '@angular/core';
import { PreloaderTypeEnum } from './preloader-types.enum';

@Component({
  selector: 'app-component-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.css']
})
export class PreloaderComponent {
  private readonly TYPES = ['default', 'mini'];
  @Input() public preloader: string = this.TYPES[PreloaderTypeEnum.DEFAULT];
  isVisible: boolean = false;

  constructor() {}

  /**
   * @param {PreloaderTypeEnum} type
   * @memberof PreloaderComponent
   */
  setType(type: PreloaderTypeEnum) {
    this.preloader = this.TYPES[type];
  }

}

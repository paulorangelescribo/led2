import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreloaderComponent } from './components/preloader/preloader.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CallerProvider } from '../../providers/caller.provider';
import { RouterModule } from '@angular/router';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { FormsModule } from '@angular/forms';
import { PromptComponent } from "./components/prompt/prompt.component";

@NgModule({
    imports: [CommonModule, FormsModule, RouterModule],
    exports: [
        PreloaderComponent,
        SearchbarComponent,
        NavbarComponent,
        DropdownComponent,
        PromptComponent
    ],
    declarations: [
        PreloaderComponent,
        SearchbarComponent,
        NavbarComponent,
        DropdownComponent,
        PromptComponent
    ],
    providers: [
        CallerProvider
    ]
})
export class GlobalComponentsModule { }

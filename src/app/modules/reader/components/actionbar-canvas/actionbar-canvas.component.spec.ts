import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionbarCanvasComponent } from './actionbar-canvas.component';

describe('ActionbarCanvasComponent', () => {
  let component: ActionbarCanvasComponent;
  let fixture: ComponentFixture<ActionbarCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionbarCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionbarCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

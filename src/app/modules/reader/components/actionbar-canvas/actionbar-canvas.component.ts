import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {BookEvent} from "../../../events/book/book.event";

@Component({
  selector: 'app-actionbar-canvas',
  templateUrl: './actionbar-canvas.component.html',
  styleUrls: ['./actionbar-canvas.component.css']
})
export class ActionbarCanvasComponent {
  private installedBookManifest: any;
  bookLedId: string;

  constructor(private router: Router, private bookEvent: BookEvent) {}

  closeCanvas() {
    this.bookEvent.EchoEnableScribble.emit(false);
    document.getElementById('page-slider').style.position = 'relative'
  }
}

import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ActionbarReaderComponent } from "./actionbar-reader.component";

describe("ActionbarReaderComponent", () => {
  let component: ActionbarReaderComponent;
  let fixture: ComponentFixture<ActionbarReaderComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ActionbarReaderComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionbarReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPubActionbarReaderComponent } from './e-pub-actionbar-reader.component';

describe('EPubActionbarReaderComponent', () => {
  let component: EPubActionbarReaderComponent;
  let fixture: ComponentFixture<EPubActionbarReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPubActionbarReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPubActionbarReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, DoCheck, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';


import {BookEvent} from './../../../events/book/book.event';
import {EPubTextformatComponent} from '../e-pub-textformat/e-pub-textformat.component';
import {EPubActionbarDropdownComponent} from '../e-pub-actionbar-dropdown/e-pub-actionbar-dropdown.component';

@Component({
  selector: 'app-e-pub-actionbar-reader',
  templateUrl: './e-pub-actionbar-reader.component.html',
  styleUrls: ['./e-pub-actionbar-reader.component.css']
})
export class EPubActionbarReaderComponent implements OnInit {
  tapped: boolean = false;
  bookId: string = '';
  aria = false;

  @ViewChild(EPubTextformatComponent)
  Textformat: EPubTextformatComponent;

  // @ViewChild(EPubActionbarDropdownComponent)
  // EPubActionbarDropdown: EPubActionbarDropdownComponent;

  constructor(private router: Router, private bookEvent: BookEvent) {
    this.bookEvent.EchoBookReader.subscribe(data => {
      this.bookId = data.bookId;
    });
    this.bookEvent.EchoHideBar.subscribe((data: boolean) => {
      this.tapped = data;
    });
    this.bookEvent.EchoAriaHiddenModal.subscribe((isHidden) => {
      this.aria = isHidden;
    });
  }

  ngOnInit() {
    this.Textformat.id = "Textcharacteristics";


    // this.EPubActionbarDropdown.setDropdownId('EPubActionbarDropdown');
  }

  openTextcharacteristics() {
    this.Textformat.show();
  }

  goToShelf() {
    this.router.navigateByUrl('/shelf');
  }

  goToEpubTextsearch() {
    // this.router.navigateByUrl('reader/' + this.bookId + '/(readers:search)');
  }

}

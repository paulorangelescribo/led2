import { Input, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';

import { BookEvent } from '../../../events/book/book.event';
import { VaultToolsService } from '../../../services/vault/vault-tools.service';

@Component({
  selector: 'app-e-pub-book-reader',
  templateUrl: './e-pub-book-reader.component.html',
  styleUrls: ['./e-pub-book-reader.component.css']
})
export class EPubBookReaderComponent implements OnInit {
  private newStyle: {} = {};

  constructor(private bookEvent: BookEvent, private tools: VaultToolsService) {}

  ngOnInit() {

    this.tools.initModals();

    this.bookEvent.EchoFormatText.subscribe(style => {

      this.newStyle['font-family'] = style.fontFamily;
      this.newStyle['font-size'] = style.fontSize;
      this.newStyle['background-color'] = style.backgroundColor;
      this.newStyle['color'] = style.color;
      this.newStyle['filter'] = style.filter;
      this.newStyle['theme'] = style.theme;

      this.bookEvent.EchoFormatTextBookSlider.emit(this.newStyle);
    });
  }
}

import { EpubDocumentService } from '../../../services/reader/epub-document.service';
import { EPubItemContract } from '../../../services/reader/contracts/e-pub-item.contract';
import { DomSanitizer } from '@angular/platform-browser';
import { PageReaderService } from '../../../services/reader/page-reader.service';
import { Component, Input, OnChanges, OnDestroy, OnInit, DoCheck, AfterContentInit, Inject } from '@angular/core';
import * as Hammer from 'hammerjs';
import { BookEvent } from '../../../events/book/book.event';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { LedFileSystemContract } from '../../../services/contracts/file-system/led-file-system.contract';
import { PlatformEnum } from '../../../services/contracts/platform/platform.enum';

@Component({
    selector: 'app-e-pub-book-slider',
    templateUrl: './e-pub-book-slider.component.html',
    styleUrls: ['./e-pub-book-slider.component.css']
})
export class EPubBookSliderComponent implements OnInit, OnDestroy, DoCheck {

    static counter: number = 0;
    private bookListLoaded: boolean = false;
    private EpubDocument: EpubDocumentService;
    private changeDetected: boolean = false;
    private epubLoaded: boolean = false;
    private loader: any;
    private isLoaded: boolean = false;

    private deviceWidth: string = window.screen.width.toString();
    private deviceHeight: number = window.screen.height;

    @Input() newStyle: {} = {};
    @Input() EpubHTML: any;
    @Input() page: number = 0;
    private tapped: boolean = false;
    private tapDetected: boolean = false;
    private anchor: string;
    aria = false;

    constructor(private sanitizer: DomSanitizer,
        private bookEvent: BookEvent,
        @Inject('PlatformService') private platform: PlatformContract,
        private PageReader: PageReaderService,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract) {

        this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl('iframe.html');

        this.onDeviceOrientation();

        $(document).ready(() => {
            this.loader = $('.fakeloader');

            this.loader.fakeLoader({
                timeToHide: 1600,
                bgColor: '#f2f2f2',
                spinner: 'spinner2',
                zIndex: 9999
            });

            $(window).resize(() => {
                this.loader.show();
                this.changeDetected = true;
            });
        });
    }

    private addEPubEventListener() {

        document.getElementById('epub-wrap').addEventListener('load', (event: any) => {
            if (event.target.src.indexOf('iframe.html') !== -1) {
                return event.preventDefault();
            }

            this.epubLoaded = false;
            this.loader.fadeOut();

            /** BIOHAZARD */
            this.EpubDocument.setPlatform(this.platform);
            this.EpubDocument.updateEpubContainer(this.deviceWidth, this.deviceHeight);

            this.EpubDocument.addSummaryEventListener(this.anchor, this.platform).then((eventClick: Event) => {
                this.resolveEPubLink(eventClick);
                this.anchor = '';
            });

            if (this.newStyle && this.newStyle.hasOwnProperty('font-size')) {
                this.EpubDocument.setStyle(this.newStyle);
            }

            this.EpubDocument.addSwipeEventListener().then((page: number) => {
                this.page = page;
                this.changeDetected = true;
            });

        });
    }

    resolveEPubLink(event: any) {
        event.preventDefault();

        const page = event.target.getAttribute('href');
        if (!page || page === "") {
            return;
        }

        if (/^https?:.*/i.test(page)) { // Link to external page, open in Browser
            this.fileSystem.openLinkExternally(page);
            return;
        }


        this.PageReader.getPageByHref(page).then(obj => {
            const anchor = obj.anchor || '';

            this.EpubDocument = new EpubDocumentService(obj.bytes, this.fileSystem.getDirectory());
            /** BIOHAZARD */
            this.EpubDocument.setPlatform(this.platform);
            this.EpubDocument.generateDocument(this.PageReader).then(blobAddress => {
                this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl(blobAddress);
                this.anchor = obj.anchor;
                this.loader.show();
            });

        }).catch(error => {
            console.log(error);
        });

        return;
    }

    private getPage(pageNumber: number) {
        return new Promise((resolve, reject) => {
            this.PageReader.processPage(pageNumber).then(resolve).catch(error => {
                if (error === 'NOT_FOUND') {
                    this.getPage(pageNumber++).then().catch(reject);
                }
            });
        });
    }

    ngOnInit() {

        this.getPage(this.page).then((bytesArray: Array<any>) => {
            this.EpubDocument = new EpubDocumentService(bytesArray, this.fileSystem.getDirectory());
            /** BIOHAZARD */
            this.EpubDocument.setPlatform(this.platform);
            this.EpubDocument.generateDocument(this.PageReader).then(blobAddress => {
                this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl(blobAddress);
            });

        }).catch(error => {
            console.log(error);
        });
        this.addEPubEventListener();

        this.bookEvent.EchoFormatTextBookSlider.subscribe((style) => {
            this.newStyle = style;

            if (!this.EpubDocument) return;

            this.EpubDocument.setStyle(style);
        });
        this.bookEvent.EchoAriaHiddenModal.subscribe((isHidden) => {
            this.aria = isHidden;
        });
    }

    ngDoCheck() {

        if (this.changeDetected) {

            this.changeDetected = false;

            this.PageReader.processPage(this.page).then(bytesArray => {
                this.EpubDocument = new EpubDocumentService(bytesArray, this.fileSystem.getDirectory());
                /** BIOHAZARD */
                this.EpubDocument.setPlatform(this.platform);
                this.EpubDocument.generateDocument(this.PageReader).then(blobAddress => {
                    this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl(blobAddress);
                    this.loader.show();
                });

            }).catch(error => {
                if (error.hasOwnProperty('error') && error.error === 'NOT_FOUND') {
                    this.page = 0;
                    this.getPage(this.page).then((bytesArray: Array<any>) => {

                        this.EpubDocument = new EpubDocumentService(bytesArray, this.fileSystem.getDirectory());
                        //** BIOHAZARD */
                        this.EpubDocument.setPlatform(this.platform);

                        this.EpubDocument.generateDocument(this.PageReader).then(blobAddress => {
                            this.EpubHTML = this.sanitizer.bypassSecurityTrustResourceUrl(blobAddress);
                        });

                    }).catch(error => {
                        //
                    });
                }
                console.log(error);
            });
        }
    }

    ngOnDestroy() {
        this.PageReader.destroy();
    }

    onDeviceOrientation() {
        window.addEventListener('orientationchange', (event) => {
            if (window.orientation === 90 || window.orientation === -90) {

                if (this.platform.is(PlatformEnum.ANDROID)) {
                    this.deviceWidth = window.screen.width.toString();
                    this.deviceHeight = window.screen.height;
                }

                if (this.platform.is(PlatformEnum.IOS)) {
                    this.deviceWidth = window.screen.height.toString();
                    this.deviceHeight = window.screen.width;
                }

            } else {

                this.deviceWidth = window.screen.width.toString();
                this.deviceHeight = window.screen.height;

            }
            const epub: any = document.getElementById('epub-wrap');
            epub.contentWindow.location.reload(true);

        });
    }
}

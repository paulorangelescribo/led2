import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPubTextcharacteristicsComponent } from './e-pub-textformat.component';

describe('EPubTextcharacteristicsComponent', () => {
  let component: EPubTextcharacteristicsComponent;
  let fixture: ComponentFixture<EPubTextcharacteristicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPubTextcharacteristicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPubTextcharacteristicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

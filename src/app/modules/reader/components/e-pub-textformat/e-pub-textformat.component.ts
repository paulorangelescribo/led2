import { Component, Input, OnInit, ViewContainerRef } from '@angular/core';

import { BookEvent } from '../../../events/book/book.event';
import { ContentManagerService } from '../../../services/content-manager/content-manager.service';
import { BookService } from '../../../services/book.service';
import { EPubTextformatThemeTypeEnum } from './e-pub-textformat-themetypes.enum';
import { EPubTextformatFontTypeEnum } from './e-pub-textformat-fonttypes.enum';
import { EPubTextformatFontTypeConst } from './e-pub-textformat-fonttype.enum';
import { ModalContract } from '../../../../contracts/modal.contract';
import { OptionItemContract } from '../../../../contracts/option-item.contract';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';


@Component({
    selector: 'app-e-pub-textformat',
    templateUrl: './e-pub-textformat.component.html',
    styleUrls: ['./e-pub-textformat.component.css']
})

export class EPubTextformatComponent implements ModalContract, OnInit {
    id: string;
    title: string = 'Características do Texto';

    @Input() public fontSize: number = 100;

    @Input() public themes: Array<OptionItemContract> = [
        { label: 'Padrão', value: EPubTextformatThemeTypeEnum.PADRAO },
        { label: 'Alto Contraste', value: EPubTextformatThemeTypeEnum.ALT0_CONTRASTE },
        { label: 'Sépia', value: EPubTextformatThemeTypeEnum.SEPIA }
    ];

    @Input() public fontFamilies: Array<OptionItemContract> = [
        { label: 'Padrão', value: EPubTextformatFontTypeEnum.PADRAO },
        { label: 'Monoespaçada', value: EPubTextformatFontTypeEnum.MONOSPACE },
        { label: 'Serifada', value: EPubTextformatFontTypeEnum.SERIF },
        { label: 'Cursiva', value: EPubTextformatFontTypeEnum.CURSIVE }
    ];

    @Input() public margins: string[] = ['Padrão', 'Pequenas', 'Grandes'];

    @Input() public lineHeightings: string[] = ['Padrão', 'Pequeno', 'Grandes'];

    @Input() public newStyle: {} = {};

    selectedTheme: OptionItemContract;
    selectedFontFamily: OptionItemContract;

    constructor(private bookEvent: BookEvent) { }

    ngOnInit() {
        $('ul span').css('color', 'black');
    }

    public emitEvent(): void {
        this.bookEvent.EchoFormatText.emit(this.newStyle);
    }

    public increment(): void {

        if (this.fontSize < 150) {
            if (document.getElementById('decrement').classList.contains('disabled')) {
                document.getElementById('decrement').classList.remove('disabled');
            }

            ++this.fontSize;
            this.newStyle['fontSize'] = this.fontSize + '%';
            this.emitEvent();

            if (this.fontSize === 150) {
                document.getElementById('increment').classList.add('disabled');
            } else if (document.getElementById('increment').classList.contains('disabled')) {
                document.getElementById('increment').classList.remove('disabled');
            }
        }
    }

    public reset(): void {

        this.fontSize = 100;
        this.newStyle['fontSize'] = this.fontSize + '%';
        this.emitEvent();

        if (document.getElementById('decrement').classList.contains('disabled')) {
            document.getElementById('decrement').classList.remove('disabled');
        }
        if (document.getElementById('increment').classList.contains('disabled')) {
            document.getElementById('increment').classList.remove('disabled');
        }
    }

    public decrement(): void {

        if (this.fontSize > 80) {
            if (document.getElementById('increment').classList.contains('disabled')) {
                document.getElementById('increment').classList.remove('disabled');
            }

            --this.fontSize;
            this.newStyle['fontSize'] = this.fontSize + '%';
            this.emitEvent();

            if (this.fontSize === 80) {
                document.getElementById('decrement').classList.add('disabled');
            } else if (document.getElementById('decrement').classList.contains('disabled')) {
                document.getElementById('increment').classList.remove('disabled');
            }
        }
    }

    selectFont() {
        switch (this.selectedFontFamily.value) {
            case EPubTextformatFontTypeEnum.PADRAO:
                this.newStyle['fontFamily'] = '';
            break;

            case EPubTextformatFontTypeEnum.MONOSPACE:
                this.newStyle['fontFamily'] = 'MONOSPACE';
            break;

            case EPubTextformatFontTypeEnum.SERIF:
                this.newStyle['fontFamily'] = 'Serif';
            break;

            case EPubTextformatFontTypeEnum.CURSIVE:
                this.newStyle['fontFamily'] = 'Comic Sans MS, Comic Sans, cursive';
            break;
        }
    }

    selectTheme() {
        this.newStyle['theme'] = this.selectedTheme.value;
        switch (this.selectedTheme.value) {
            case EPubTextformatThemeTypeEnum.PADRAO:
                this.newStyle['backgroundColor'] = 'white';
                this.newStyle['color'] = 'inherit';
                this.newStyle['filter'] = 'none';
            break;

            case EPubTextformatThemeTypeEnum.ALT0_CONTRASTE:
                this.newStyle['backgroundColor'] = 'black';
                this.newStyle['color'] = 'inherit';
                this.newStyle['mixBlendMode'] = 'difference';
                this.newStyle['filter'] = 'invert() contrast()';
            break;

            case EPubTextformatThemeTypeEnum.SEPIA:
                this.newStyle['backgroundColor'] = 'white';
                this.newStyle['mixBlendMode'] = 'difference';
                this.newStyle['filter'] = 'sepia(80%)';
            break;
        }
    }

    submit(selectedFontFamily: string, selectedTheme: string) {
        this.selectedFontFamily = JSON.parse(selectedFontFamily);
        this.selectedTheme = JSON.parse(selectedTheme);
        this.selectFont();
        this.selectTheme();
        this.emitEvent();
    }

    show() {
        $(`#${this.id}`).modal('open');
        $('select').material_select();
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }
}

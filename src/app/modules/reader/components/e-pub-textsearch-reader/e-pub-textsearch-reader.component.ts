import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-e-pub-textsearch-reader',
  templateUrl: './e-pub-textsearch-reader.component.html',
  styleUrls: ['./e-pub-textsearch-reader.component.css']
})
export class EPubTextsearchReaderComponent implements OnInit {
  goToEpub() {
    this.router.navigateByUrl('');
  }
  startSearch() {}
  stopSearch() {}

  constructor(private router: Router) {}

  ngOnInit() {}
}

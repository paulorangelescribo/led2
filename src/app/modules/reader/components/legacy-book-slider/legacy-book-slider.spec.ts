import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookAjaxSliderComponent } from './book-ajax-slider.component';

describe('BookAjaxSliderComponent', () => {
  let component: BookAjaxSliderComponent;
  let fixture: ComponentFixture<BookAjaxSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookAjaxSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookAjaxSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

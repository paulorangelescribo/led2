import { Component, OnInit } from '@angular/core';
import { UserSettingsService } from '../../../services/user-settings.service';
import { Input } from '@angular/core';
import {BookEvent} from "../../../events/book/book.event";

@Component({
  selector: 'app-page-marker',
  templateUrl: './page-marker.component.html',
  styleUrls: ['./page-marker.component.css']
})
export class PageMarkerComponent implements OnInit {
  @Input() pageMarkerImage: string = '';

  private isEnabled = false;
  private currentBookId: string;
  private currentPageNumber: number;

  constructor(private userSettings: UserSettingsService, private bookEvent: BookEvent) {

  }

  ngOnInit() {
    this.currentBookId = null;
    this.currentPageNumber = null;
    this.isEnabled = false;
    this.updateImage();
  }

  setSelectedPage(bookId: string, pageNumber: number) {

    this.currentBookId = bookId;
    this.currentPageNumber = pageNumber;

    this.isEnabled = this.userSettings.getBookmarkStatus(this.currentBookId, this.currentPageNumber);
    this.updateImage();
  }

  toggleMarker() {
    this.isEnabled = !this.isEnabled;
    this.userSettings.setBookmarkStatus(this.currentBookId, this.currentPageNumber, this.isEnabled);
    this.bookEvent.EchoUpdateBookmarkSummary.emit(true);
    this.updateImage();
  }

  private updateImage() {
    if (this.isEnabled) {
      this.pageMarkerImage = 'assets/images/pagemarker_enabled.png';
    } else {
      this.pageMarkerImage = 'assets/images/pagemarker_disabled.png';
    }
  }

}

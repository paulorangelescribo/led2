import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BookEvent} from '../../../events/book/book.event';
import { ModalContract } from '../../../../contracts/modal.contract';

@Component({
  selector: 'app-resize-rabisco',
  templateUrl: './resizerabisco.component.html',
  styleUrls: ['./resizerabisco.component.css']
})

export class ResizeRabiscoComponent implements OnInit, OnDestroy, ModalContract {
    id: string = 'ResizeRabiscoComponent';
    title: string = 'Apresentação';
  resizeLever: HTMLElement|any;

  constructor(private bookEvent: BookEvent) {}

  ngOnInit() {
    this.resizeLever = document.getElementById('lever');
    this.resizeLever.checked = false;

    this.bookEvent.EchoCourtainOnSlide.subscribe((emitted) => {
      if ( this.resizeLever.checked ) {
        this.bookEvent.EchoCourtain.emit(true);
      }
    });

    this.bookEvent.EchoStartUserHotspot.subscribe(_ => {
      this.resizeLever.checked = false;
    });
  }

  slideLever() {
    if (!this.resizeLever.checked) {
      this.bookEvent.EchoCourtain.emit(false);
    } else {
      this.bookEvent.EchoCourtain.emit(true);
    }
  }
  openScribble() {
    if (this.resizeLever.checked) {
      this.resizeLever.checked = false;
      this.bookEvent.EchoCourtain.emit(false);
    }
    this.hide();
    this.bookEvent.EchoEnableScribble.emit(true);
  }
  ngOnDestroy() {
    if (this.resizeLever != null) {
        this.resizeLever.checked = false;
    }
    this.bookEvent.EchoCourtain.emit(false);
  }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }
}

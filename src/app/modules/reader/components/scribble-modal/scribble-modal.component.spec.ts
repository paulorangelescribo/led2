import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScribbleModalComponent } from './scribble-modal.component';

describe('ScribbleModalComponent', () => {
  let component: ScribbleModalComponent;
  let fixture: ComponentFixture<ScribbleModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScribbleModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScribbleModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnInit} from '@angular/core';
import {BookEvent} from "../../../events/book/book.event";
import { ModalContract } from '../../../../contracts/modal.contract';

@Component({
  selector: 'app-canvas-modal',
  templateUrl: './scribble-modal.component.html',
  styleUrls: ['./scribble-modal.component.css']
})

export class ScribbleModalComponent implements OnInit, ModalContract {
    id: string;
    title: string;

  @Input() public htmlElement: HTMLElement = <HTMLElement>document.body;

  constructor(private bookEvent: BookEvent) {}

  apply() {
    Array.from(document.querySelectorAll('.active')).forEach((element, index) => {
      this.bookEvent.EchoCanvasSettings.emit(element.getAttribute('data-config'));
    });
  }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

  ngOnInit() {
    this.htmlElement.querySelector(".red")
      .addEventListener("click", () => {
        this.htmlElement.querySelector(".colors .active").classList.remove('active')
        this.htmlElement.querySelector(".red").classList.add('active')
      });

    this.htmlElement.querySelector(".blue")
      .addEventListener("click", () => {
        this.htmlElement.querySelector(".colors .active").classList.remove('active')
        this.htmlElement.querySelector(".blue").classList.add('active')
      });

    this.htmlElement.querySelector(".green")
      .addEventListener("click", () => {
        this.htmlElement.querySelector(".colors .active").classList.remove('active')
        this.htmlElement.querySelector(".green").classList.add('active')
      });


    this.htmlElement.querySelector(".smaller")
      .addEventListener("click", () => {
        this.htmlElement.querySelector(".thickness .active").classList.remove('active')
        this.htmlElement.querySelector(".smaller").classList.add('active')
      });

    this.htmlElement.querySelector(".normal")
      .addEventListener("click", () => {
        this.htmlElement.querySelector(".thickness .active").classList.remove('active')
        this.htmlElement.querySelector(".normal").classList.add('active')
      });

    this.htmlElement.querySelector(".large")
      .addEventListener("click", () => {
        this.htmlElement.querySelector(".thickness .active").classList.remove('active')
        this.htmlElement.querySelector(".large").classList.add('active')
      });
  }
}

import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import {BookEvent} from '../../../events/book/book.event';
import { PlatformContract } from '../../../services/contracts/platform/platform.contract';
import { PlatformEnum } from '../../../services/contracts/platform/platform.enum';

@Component({
  selector: 'app-scribble',
  templateUrl: './scribble.component.html',
  styleUrls: ['./scribble.component.css']
})
export class ScribbleComponent implements OnInit, OnDestroy {

  top: number;

  canvasScribble: any;
  ctx: any;

  touch: any;

  curX: number;
  curY: number;
  curSize: string;
  curColor: string;
  curTool: string;
  dragging: boolean = false;
  paint: boolean;

  constructor(private bookEvent: BookEvent,
    @Inject('PlatformService') private platform: PlatformContract) {
  }

  private initCanvas() {

    this.canvasScribble.width = this.canvasScribble.clientWidth;
    this.canvasScribble.height = this.canvasScribble.clientHeight;

    this.ctx = this.canvasScribble.getContext('2d');
    this.curTool = 'crayon';
    this.curColor = '#ff0000';
    this.curSize = 'normal';
  }

  private redraw() {

    Object.assign(this.ctx, {
      lineJoin: 'round',
      strokeStyle: this.curColor,
      lineWidth: ({
        small: 2,
        normal: 5,
        large: 10,
        huge: 20
      })[this.curSize] || 0
    });

    this.ctx.lineTo(this.curX, this.curY);
    this.ctx.closePath();
    this.ctx.stroke();
  }

  private erasing() {
    this.ctx.clearRect(this.curX, this.curY, 30, 30);
  }

  private eraseOrRedraw() {
    if (this.curTool === 'eraser') {
      this.erasing();
    } else {
      if (this.dragging)
        this.redraw();
      this.ctx.beginPath();
      this.ctx.moveTo(this.curX, this.curY);
    }
  }

  private getGesturesPositions(touch) {
    const width = this.canvasScribble.clientWidth / this.canvasScribble.width;
    const height = this.canvasScribble.clientHeight / this.canvasScribble.height;
    let parent = this.canvasScribble.parentElement;
    let offsetX = 0, offsetY = 0;
    while (parent) {
      offsetY += parent.scrollTop;
      offsetX += parent.scrollLeft;
      parent = parent.parentElement;
    }

    if (this.platform.is(PlatformEnum.DESKTOP)) {
      this.curX = (offsetX + touch.layerX) / width;
      this.curY = (offsetY + touch.layerY) / height;
    } else if (this.platform.is(PlatformEnum.MOBILE)) {
      this.curX = (offsetX + touch.clientX) / width;
      this.curY = (offsetY + touch.clientY) / height;
    }

  }

  ngOnInit() {

    this.bookEvent.EchoEnableHammerEvents.emit(false);

    this.canvasScribble = document.getElementById('canvasScribble');

    this.initCanvas();

    ['touchstart', 'mousedown'].forEach(event => this.canvasScribble.addEventListener(event, (e) => {

      e.stopPropagation();
      e.preventDefault();

      let touch;

      if (e.touches) {
        touch = e.touches[0];
      } else if (e) {
        touch = e;
      }

      this.getGesturesPositions(touch);
      this.paint = true;
      this.dragging = false;
      this.eraseOrRedraw();
    }));

    ['touchmove', 'mousemove'].forEach(event => this.canvasScribble.addEventListener(event, (e) => {

      e.stopPropagation();
      e.preventDefault();

      let touch;

      if (e.touches) {
        touch = e.touches[0];
      } else if (e) {
        touch = e;
      }

      this.getGesturesPositions(touch);
      if (this.paint) {
        this.dragging = true;
        this.eraseOrRedraw();
      }
    }));

    ['touchend', 'mouseup'].forEach(event => this.canvasScribble.addEventListener(event, (e) => {

      this.ctx.closePath();
      this.paint = false;
    }));

    ['touchcancel', 'mouseleave'].forEach(event => this.canvasScribble.addEventListener(event, (e) => {

      this.ctx.closePath();
      this.paint = false;
    }));

    this.bookEvent.EchoCanvasSettings.subscribe(setting => {

      if (setting === 'small' || setting === 'normal' || setting === 'large') {
        this.canvasScribble.classList.remove('eraserDesktop');
        this.canvasScribble.classList.add('brushDesktop');
        this.curSize = setting;
        this.curTool = 'crayon';
      } else if (setting === '#ff0000' || setting === '#008000' || setting === '#0000ff') {
        this.canvasScribble.classList.remove('eraserDesktop');
        this.canvasScribble.classList.add('brushDesktop');
        this.curColor = setting;
        this.curTool = 'crayon';
      } else if (setting === 'eraser') {
        this.canvasScribble.classList.remove('brushDesktop');
        this.canvasScribble.classList.add('eraserDesktop');
        this.curTool = setting;
      } else if (setting === 'clear') {
        this.ctx.clearRect(0, 0, this.canvasScribble.width, this.canvasScribble.height);
      }
    });
  }

  ngOnDestroy() {
    this.bookEvent.EchoEnableHammerEvents.emit(true);
  }
}

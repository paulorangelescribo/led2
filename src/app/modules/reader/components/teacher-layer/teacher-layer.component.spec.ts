import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherLayerComponent } from './teacher-layer.component';

describe('TeacherLayerComponent', () => {
  let component: TeacherLayerComponent;
  let fixture: ComponentFixture<TeacherLayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherLayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

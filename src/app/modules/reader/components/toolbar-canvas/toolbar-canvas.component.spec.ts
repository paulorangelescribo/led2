import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarCanvasComponent } from './toolbar-canvas.component';

describe('ToolbarCanvasComponent', () => {
  let component: ToolbarCanvasComponent;
  let fixture: ComponentFixture<ToolbarCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

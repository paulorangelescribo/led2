import {Component, OnInit, ViewChild} from '@angular/core';
import {BookEvent} from '../../../events/book/book.event';
import {ScribbleModalComponent} from '../scribble-modal/scribble-modal.component';

@Component({
  selector: 'app-toolbar-canvas',
  templateUrl: './toolbar-canvas.component.html',
  styleUrls: ['./toolbar-canvas.component.css']
})
export class ToolbarCanvasComponent implements OnInit {
  @ViewChild(ScribbleModalComponent) ScribbleModal: ScribbleModalComponent;

  constructor(private bookEvent: BookEvent) {
  }

  openCanvasModal() {
    this.ScribbleModal.show();
  }

  eraser() {
    this.bookEvent.EchoCanvasSettings.emit('eraser');
  }

  clear() {
    this.bookEvent.EchoCanvasSettings.emit('clear');
  }

  ngOnInit() {
    this.ScribbleModal.id = "CanvasModal";
  }

}

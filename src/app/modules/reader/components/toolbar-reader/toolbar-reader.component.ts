import { PaginationComponent } from '../pagination/pagination.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SummaryComponent } from '../summary/summary.component';
import { AnnotationComponent } from '../annotation/annotation.component';
import { PresentationComponent } from '../presentation/presentation.component';
import { BookEvent } from '../../../events/book/book.event';
import { ResizeRabiscoComponent } from '../resizerabisco/resizerabisco.component';
import { environment } from '../../../../../environments/environment';
import { ContentManagerService } from '../../../services/content-manager/content-manager.service';

@Component({
    selector: 'app-toolbar-reader',
    templateUrl: './toolbar-reader.component.html',
    styleUrls: ['./toolbar-reader.component.css']
})
export class ToolbarReaderComponent implements OnInit {

    @ViewChild(SummaryComponent) Summary: SummaryComponent;
    @ViewChild(AnnotationComponent) Annotation: AnnotationComponent;
    @ViewChild(PaginationComponent) Pagination: PaginationComponent;
    @ViewChild(PresentationComponent) Presentation: PresentationComponent;
    @ViewChild(ResizeRabiscoComponent) ResizeRabisco: ResizeRabiscoComponent;
    tapped: boolean = false;
    teacherButtonEnabled: boolean = false;
    constructor(private bookEvent: BookEvent, private contentManager: ContentManagerService) {
        this.bookEvent.EchoHideBar.subscribe((tapped) => {
            this.tapped = tapped;
        });

        this.bookEvent.EchoTeacherLayerAvailable.subscribe(available => {
            let teacherBtn = document.getElementById('teacherButton');

            if (available) {
                teacherBtn.style.display = 'block';
            } else {
                teacherBtn.style.display = 'none';
            }
        });
    }

    ngOnInit() {
        this.Summary.id = "SummaryComponent";
        this.Annotation.id = "AnnotationComponent";
        this.Pagination.id = "PaginationComponent";
        this.Presentation.id = "PresentationComponent";
        this.ResizeRabisco.id = "ResizeRabiscoComponent";
    }

    openSummary() {
        this.Summary.show();
    }

    openAnnotation() {
        if (environment.client != 'alfacon') {
            this.bookEvent.EchoStartUserHotspot.emit();
        }
        else {
            this.Annotation.show();
        }
    }

    openPagination() {
        this.Pagination.show();
    }

    toggleTeacherLayer() {
        this.teacherButtonEnabled = !this.teacherButtonEnabled;
        let materializeIconTxt = document.getElementById('teacherLayerIconTxt');
        if (this.teacherButtonEnabled == true) {
            materializeIconTxt.textContent = 'layers';
        }
        else {
            materializeIconTxt.textContent = 'layers_clear';
        }
        this.bookEvent.EchoToggleTeacherLayer.emit(this.teacherButtonEnabled);
    }

    openPresentation() {
        this.Presentation.show();
    }
    openCortina() {
        this.ResizeRabisco.show();
    }

}

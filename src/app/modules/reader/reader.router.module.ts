import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ShelfComponent } from "../shelf/components/shelf/shelf.component";
import { AuthGuardService } from "../services/guard/auth-guard.service";
import { MainReaderComponent } from "./components/main-reader/main-reader.component";
import { LegacyBookReaderComponent } from "./components/legacy-book-reader/legacy-book-reader.component";
import { EPubBookReaderComponent } from "./components/e-pub-book-reader/e-pub-book-reader.component";
import { EPubTextsearchReaderComponent } from "./components/e-pub-textsearch-reader/e-pub-textsearch-reader.component";

const readersRoute: Routes = [
    {
        path: ":id",
        component: MainReaderComponent,
        children: [
            {
                path: "legacy",
                component: LegacyBookReaderComponent,
                outlet: "readers"
            },
            {
                path: "epub",
                component: EPubBookReaderComponent,
                outlet: "readers"
            },
            {
                path: "search",
                component: EPubTextsearchReaderComponent,
                outlet: "readers"
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(readersRoute)],
    exports: [RouterModule]
})
export class ReaderRouterModule {}

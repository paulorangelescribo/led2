import { Injectable, EventEmitter, Inject } from '@angular/core';
import { WebWorkerService } from 'angular2-web-worker';

import { LegacyBookContract } from '../../../contracts/legacy-book.contract';
import { LogProvider } from '../../../providers/log.provider';
import { AppEvent } from '../../events/app.event';
import { DownloadManagerService } from './download-manager.service';
import { InstallerService } from './installer.service';
import { DownloadableObject } from './downloadable-object';
import { BookDao } from '../../daos/book.dao';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';

@Injectable()
export class ContentManagerService {
    private managedBookSet: any;
    private bookStatusData: any;
    private Log: LogProvider = LogProvider.getInstance("ContentManagerService");
    private currentDownloadingBookId: string;
    private pageRequest: { bookId: string, pageId: number, callback: Function };


    constructor(
        private installer: InstallerService,
        private downloadManager: DownloadManagerService,
        private appEvent: AppEvent,
        @Inject('PlatformService') private platform: PlatformContract,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private book: BookDao
    ) {
        this.managedBookSet = {};
        this.initEventProcessing();
        this.book.ContentManager = this;
    }

    private initEventProcessing() {
        this.appEvent.EchoDownloadObjectStart.subscribe(downloadableObj => {
            this.currentDownloadingBookId = downloadableObj.book_id;

            let bookProgress = this.getBookProgress(downloadableObj.book_id);
            this.appEvent.EchoProgress.emit(bookProgress);
        });

        // init event listener for DownloadManager events
        this.appEvent.EchoDownloadObjectFinish.subscribe(downloadableObj => {
            let bookId = downloadableObj.book_id;
            let pageId = downloadableObj.page_id;

            let bookProgress = this.getBookProgress(bookId);

            if (this.pageRequest != null &&
                (this.pageRequest.bookId == bookId) &&
                (this.pageRequest.pageId == pageId)) {
                let pageStatus = this.getPageStatus(bookId, pageId);
                this.pageRequest.callback(bookId, pageId, pageStatus);
                this.pageRequest = null;
            }

            this.saveDownloadProgress(downloadableObj.book_id)
                .then(async () => {
                    this.appEvent.EchoProgress.emit(bookProgress);

                    this.bookStatusData[bookId] = bookProgress;

                    this.currentDownloadingBookId = null;

                    try {
                        await this.saveBookStatusData();
                    }
                    catch (error) {
                        console.log("Não foi possível salvar as informações de status de livro" + bookId +
                            "! Erro: " + JSON.stringify(error))
                    }
                })
                .catch(error => {
                    this.Log.save(
                        "Não foi possível salvar informações de download do livro " +
                        bookId +
                        "! Erro: " +
                        JSON.stringify(error)
                    );
                });
        });

        this.appEvent.EchoSpaceRequestForObject.subscribe((objectToInstall: DownloadableObject) => {
            this.requestSpaceForBook(objectToInstall.book_id, (objectToInstall.compressed_size | 0) + (objectToInstall.exported_filesize_uncompressed | 0) + 5000000).then(() => {
                console.log('succesfully tried to free some space');
            }).catch((error) => {
                console.log('error while trying to free space: ' + JSON.stringify(error));
            })
        });
    }

    private getPageStatus(bookId: string, pageId: number) {

        let downloadManifest = this.managedBookSet[bookId];

        let pageImg = downloadManifest.objectsByPageId[pageId].page_img;

        let status = pageImg.status;

        return status;

    }

    loadBookStatusData() {
        if (this.bookStatusData == null) {
            this.fileSystem.request("book-status-data.json")
                .readFile()
                .toJSON(booksData => {
                    this.bookStatusData = booksData;
                    this.resumeBookDownloads();
                    this.notifyBookStatusData();
                }, error => {
                    this.bookStatusData = {};
                    console.log("arquivo book-status-data.json ainda inexistente");
                });
        } else {
            this.resumeBookDownloads();
            this.notifyBookStatusData();
        }
    }

    private notifyBookStatusData() {
        // para cada livro, envia a notificação de seu status
        for (let bookId in this.bookStatusData) {
            if (this.bookStatusData.hasOwnProperty(bookId)) {
                this.appEvent.EchoProgress.emit(this.bookStatusData[bookId]);
            }
        }
    }

    private resumeBookDownloads() {
        // para cada livro, envia a notificação de seu status
        for (let bookId in this.bookStatusData) {
            if (this.bookStatusData.hasOwnProperty(bookId)) {
                this.resumeDownload(bookId);
            }
        }
    }

    public hasTeacherLayer(bookId: string) {
        let downloadManifest = this.managedBookSet[bookId];
        return downloadManifest.teacher_layer != null;
    }

    public isTeacherLayerInstalled(bookId: string) {
        let downloadManifest = this.managedBookSet[bookId];
        let installed = false;

        if (this.hasTeacherLayer(bookId)) {
            installed = (downloadManifest.teacher_layer.status == 'instalado')
        }

        return installed;
    }

    private getBookProgress(bookId: any) {
        let downloadManifest = this.managedBookSet[bookId];
        let bookProgress = {
            book_id: downloadManifest.book_id,
            book_status: this.getBookStatus(downloadManifest),
            book_downloading_now:
                downloadManifest.book_id == this.currentDownloadingBookId,
            book_progress: this.getDownloadPercentual(downloadManifest),
            book_version: downloadManifest.book_version
        };
        return bookProgress;
    }

    private getBookUsedSize(bookId: string, considerObligatoryBocks: boolean): number {
        let totalSize = 0;
        let downloadManifest = this.managedBookSet[bookId];

        for (let pageId in downloadManifest.objectsByPageId) {
            if (!considerObligatoryBocks && (parseInt(pageId) == 1)) {
                continue;
            }
            let obj: DownloadableObject = downloadManifest.objectsByPageId[pageId];
            if (obj.status == 'instalado') {
                totalSize += (obj.exported_filesize_uncompressed | 0);
            }
        }

        if ((downloadManifest.teacher_layer != null) && downloadManifest.teacher_layer.status == 'instalado') {
            totalSize += (downloadManifest.teacher_layer.exported_filesize_uncompressed | 0);
        }
        return totalSize;
    }

    private getDownloadPercentual(downloadManifest) {
        let totalObjects = 0;
        let downloadedObjects = 0;

        for (let pageId in downloadManifest.objectsByPageId) {
            let page = downloadManifest.objectsByPageId[pageId];
            let pageObj = page.page_img;
            if (pageObj.status === "instalado") {
                downloadedObjects++;
            }
            totalObjects++;
        }

        return downloadedObjects / totalObjects;
    }

    public setLastAccesedTime(bookId: number) {

        this.getBookStatusData().then((bookStatusData) => {
            if (bookStatusData != null) {
                let currentBookData = bookStatusData[bookId];
                if (currentBookData != null) {

                    currentBookData.last_acccessed = Date.now();
                }
            }
        }).catch(() => {
            console.log('error getting bookStatusData to set lastAccessedTime for book!');
        });

    }

    public getBookAccessAndSizeData(): Array<{ usedSpace: number, bookId: string, lastAccess: number }> {
        let data = new Array<{ usedSpace: number, bookId: string, lastAccess: number }>();
        for (let bookId in this.bookStatusData) {
            let bookData = {
                bookId: bookId,
                usedSpace: this.getBookUsedSize(bookId, false),
                lastAccess: (this.bookStatusData[bookId].last_acccessed | 0)
            };
            data.push(bookData);
        }
        data = data.sort((a, b) => {
            return a.lastAccess - b.lastAccess;
        });

        return data;
    }

    public requestSpaceForBook(bookToInstallId: string, requiredSpace: number) {
        return new Promise<boolean>((resolve, reject) => {
            console.log('requestSpaceForBook called');
            let bookAccessData = this.getBookAccessAndSizeData();
            let i = 0;
            let freedSpace = 0;
            let hasError = false;


            let next = () => {
                let currentBookId;
                if (bookAccessData[i] != null) {
                    currentBookId = bookAccessData[i].bookId;

                    if (bookToInstallId == currentBookId) {
                        i++;

                        if (i < bookAccessData.length) {
                            next();
                        }
                    } else {
                        console.log('requestSpaceForBook: will now call file deletion for book_id = ' + currentBookId);
                        this.deleteBookNonObligatoryObjects(currentBookId).then(() => {
                            freedSpace += bookAccessData[i].usedSpace;
                            i++;

                            if ((i < bookAccessData.length) && (freedSpace < requiredSpace)) {
                                next();
                            }
                            else {
                                resolve();
                            }
                        }).catch((error) => {
                            console.log('error on method requestSpaceForBook while calling deleteBookNonObligatoryObjects: ' + JSON.stringify(error));
                            reject();
                        });
                    }
                }
                else {
                    resolve(false);
                }
            }

            next();
        });


    }

    public deleteBookNonObligatoryObjects(bookId: string): Promise<boolean> {

        return new Promise<boolean>((resolve, reject) => {
            let downloadManifest = this.managedBookSet[bookId];
            if (downloadManifest != null) {

                let pageIds = Object.keys(downloadManifest.objectsByPageId);
                let i = 0;
                let next = () => {
                    let pageId = parseInt(pageIds[i]);
                    if (pageId != 1) {
                        let obj = downloadManifest.objectsByPageId[pageId];

                        this.deletePageObjects(obj).then(() => {
                            i++;
                            if (i < pageIds.length) {
                                next();
                            }
                            else {

                                this.saveDownloadProgress(bookId).then(() => {
                                    let bookProgress = this.getBookProgress(bookId);
                                    this.appEvent.EchoProgress.emit(bookProgress);

                                    this.bookStatusData[bookId] = bookProgress;

                                    resolve();
                                }).catch((error) => {
                                    let errorStr = 'error calling saveDownloadManifest (on method deleteBookNonObligatoryObjects) for object: ' + JSON.stringify(obj);
                                    reject(errorStr);
                                });
                            }
                        }).catch((error) => {
                            let errorStr = 'error calling deletePageObjects (on method deleteBookNonObligatoryObjects) for object: ' + JSON.stringify(obj);
                            reject(errorStr);
                        });
                    }
                    else {
                        i++;
                        if (i < pageIds.length) {
                            next();
                        }
                    }
                }

                next();

            }
        });
    }

    private deletePageObjects(pageObj: { page_img: DownloadableObject, oeds: Array<DownloadableObject> }): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            this.installer.deletePage(pageObj.page_img).then(() => {
                pageObj.page_img.status = 'parado';

                if (pageObj.oeds.length == 0) {
                    resolve();
                }
                else {
                    let i = 0;
                    let next = () => {
                        let oedObj = pageObj.oeds[i];
                        this.installer.uninstallOED(oedObj).then(() => {
                            oedObj.status = 'parado';
                            i++;

                            if (i < pageObj.oeds.length) {
                                next();
                            }
                            else {
                                resolve();
                            }

                        }).catch((error) => {
                            let errorStr = 'error deleting OED object (deletePageObjects): ' + JSON.stringify(error);
                            reject(errorStr);
                        });
                    }
                    next();
                }
            }).catch(reject);

        });
    }

    private getBookStatus(bookManifest: any): string {
        let bookStatus = "baixado";

        for (let pageId in bookManifest.objectsByPageId) {
            let page = bookManifest.objectsByPageId[pageId];
            let downloadableObjects = [];

            downloadableObjects.push(page.page_img);
            for (let oed of page.oeds) {
                downloadableObjects.push(oed);
            }
            if (bookManifest.teacher_layer != null) {
                downloadableObjects.push(bookManifest.teacher_layer)
            }

            for (let object of downloadableObjects) {
                let objStatus = object.status;

                if ((object == page.page_img) && (object.page_id == 1) && (objStatus != "instalado")) {
                    bookStatus = "inicializando";
                    break;
                }

                if (bookStatus != "inicializando") {
                    if (objStatus == "baixando") {
                        bookStatus = "baixando";
                    }
                    if (objStatus == "completo") {
                        bookStatus = "baixando";
                    }
                    if (objStatus == "parado") {
                        bookStatus = "baixando";
                    }
                } else {
                    break;
                }
            }
        }

        return bookStatus;
    }

    public async pollPageStatus(bookId: string, pageId: number, listener: Function) {

        let currentObjStatus = this.getPageStatus(bookId, pageId);

        if (currentObjStatus != 'instalado') {
            await this.resumeDownload(bookId);
            this.downloadManager.prioritize(bookId, pageId);
            this.pageRequest = { bookId: bookId, pageId: pageId, callback: listener };
        }
        else {
            this.pageRequest = null;
            listener(bookId, pageId, currentObjStatus);
        }
    }

    onReceiveBookManifest(bookId: string, legacyManifest: any, cdnURLs: any) {

        this.fileSystem
            .request(bookId + ".download.manifest.json")
            .readFile()
            .toJSON(manifest => {

                this.managedBookSet[bookId] = manifest;
                this.downloadBook(manifest);

            }, error => {

                let downloadManifest = this.getDownloadManifest(legacyManifest, bookId, cdnURLs);

                this.managedBookSet[bookId] = downloadManifest;
                this.downloadBook(this.managedBookSet[bookId]);

                this.saveDownloadProgress(bookId).then(() => {
                    this.downloadBook(downloadManifest);
                })
                    .catch(errorManifest => {
                        Materialize.toast(JSON.stringify(errorManifest), 5000);
                    });
            });

    }

    async saveFilesManifest(bookLedId: string, manifestFile: any) {
        return new Promise((resolve, reject) => {
            this.fileSystem
                .request(`${bookLedId}.files.manifest.json`)
                .writeFile(new Buffer(JSON.stringify(manifestFile)), resolve, reject);
        });
    }

    async getFilesManifest(bookLedId: string) {
        return new Promise((resolve, reject) => {
            this.fileSystem.exists(`${bookLedId}.files.manifest.json`, (exists) => {
                if (exists) {
                    this.fileSystem.request(`${bookLedId}.files.manifest.json`)
                        .readFile()
                        .toJSON(resolve, reject);
                } else {
                    throw Error('Arquivo não existe.');
                }
            });
        });
    }

    private async saveBookStatusData(): Promise<any> {

        return new Promise((resolve, reject) => {
            const data = new Buffer(JSON.stringify(this.bookStatusData));
            this.fileSystem.request('book-status-data.json').writeFile(data, resolve, reject);
        });
    }

    private saveDownloadProgress(bookId) {

        let filename = bookId + ".download.manifest.json";
        let dynamicManifest = this.managedBookSet[bookId];
        return new Promise((resolve, reject) => {
            this.fileSystem.request(filename).writeFile(JSON.stringify(dynamicManifest), resolve, reject);
        });
    }

    async resumeDownload(bookId: string) {

        try {
            let manifest = await this.loadDownloadProgressData(bookId);

            let statusData = this.bookStatusData[bookId];
            if (statusData != null && statusData.book_status != "baixado") {
                this.downloadBook(manifest);
            }
        }
        catch (error) {
            console.log(
                "erro baixando manifesto - download não pode ser continuado. Livro não instalado?"
            );
        }
    }

    downloadBook(manifest: any) {

        let bookObjects = this.getBookDownloadableObjects(manifest);
        if (this.bookStatusData != null && this.bookStatusData[manifest.book_id] != null) {
            this.appEvent.EchoProgress.emit(this.bookStatusData[manifest.book_id]);
        }

        this.downloadManager.addBookObjectsToDownload(manifest.book_id, manifest.cdnURLs, bookObjects);
    }

    private getBookDownloadableObjects(downloadManifest): Array<DownloadableObject> {
        const objectsByPageId = downloadManifest.objectsByPageId;

        const bookObjects: Array<DownloadableObject> = [];

        if (downloadManifest.book_format == 'ledEpub3_v1') {
            let epubObj = objectsByPageId["1"];
            epubObj.page_img.obj_type = 'E';
            bookObjects.push(epubObj.page_img);
        } else {
            for (let pageId in objectsByPageId) {
                let page = objectsByPageId[pageId];

                let pageImg = page.page_img;
                pageImg.obj_type = "P";

                if (pageImg.status !== "instalado" && pageImg.status !== "completo") {
                    bookObjects.push(pageImg);
                }

                for (let oed of page.oeds) {
                    oed.obj_type = "O";
                    if (oed.status !== "instalado" && oed.status !== "completo") {
                        bookObjects.push(oed);
                    }
                }
            }

            let teacherObj = downloadManifest.teacher_layer;
            if (teacherObj != null && teacherObj.status != 'instalado') {
                bookObjects.push(teacherObj);
            }
        }

        return bookObjects;
    }

    private loadDownloadProgressData(bookId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.managedBookSet[bookId] == null) {
                const file = bookId + '.download.manifest.json';
                this.fileSystem.request(file)
                    .readFile()
                    .toJSON((obj) => {
                        this.managedBookSet[bookId] = obj;
                        resolve(obj);
                    }, reject);
            }
            else {
                resolve(this.managedBookSet[bookId]);
            }

        });
    }

    clearDownloads(): void {
        this.downloadManager.cleanQueue();
    }

    getBookStatusData(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.bookStatusData) {
                return resolve(this.bookStatusData);
            }

            this.fileSystem.request('book-status-data.json')
                .readFile()
                .toJSON(resolve, () => reject([]));
        });
    }

    async removeBookStatusData(bookLedId: string) {
        if (this.bookStatusData[bookLedId]) {
            delete this.bookStatusData[bookLedId];
            try {
                await this.saveBookStatusData();
            }
            catch (error) {
                console.log("removeBookStatusData -> saveBookStatusData (" + error + ")");
            }
        }

        this.getBookStatusData()
            .then(async (bookStatusData: any) => {
                this.bookStatusData[bookLedId] = bookStatusData[bookLedId];
                delete this.bookStatusData[bookLedId];
                await this.saveBookStatusData();
            })
            .catch(error => {
                this.Log.save(error, "getBookStatusData -> removeBookStatusData");
            });
    }

    uninstallBook(bookLedId: string) {
        this.fileSystem
            .request()
            .readDir('utf8')
            .then(files => {
                let promises = [];
                for (let file of files) {
                    if (typeof file === 'string' && file.indexOf(bookLedId) !== -1) {
                        if (file !== `${bookLedId}.files.manifest.json`) promises.push(this.fileSystem.request(file).delete());
                    }
                }
                if (!promises.length) return;

                Promise.all(promises)
                    .then(() => Materialize.toast("Desinstalação de obra concluída", 3000))
                    .catch(error => {
                        Materialize.toast("Desinstalação de obra concluída", 3000);
                        console.log(`Falha ao desinstalar obra [${error}]`);
                    });

                this.removeBookStatusData(bookLedId);
            }).catch(error => console.log(error));
    }

    /**
     * Get installed book manifest
     * @param bookLedId
     */
    getInstalledBookManifest(bookLedId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.fileSystem.request(bookLedId + "/installed_book_manifest.json")
                .readFile()
                .toJSON(json => {
                    json['page_id_number_map'] = this.getBookPageIdToNumberMap(bookLedId);
                    json['page_number_id_map'] = this.getBookPageNumberToIdMap(bookLedId);
                    resolve(json);
                }, reject);
        });
    }

    /**
     * Get book from bookList
     * @param bookLedId
     */
    getBook(bookLedId: string): Promise<LegacyBookContract> {
        return this.book.findById(bookLedId);
    }

    public getBookPageIdToNumberMap(bookId: string) {
        let pageIdToNumberMap = {};
        let downloadManifest = this.managedBookSet[bookId];

        if (downloadManifest != null) {

            for (let pageId in downloadManifest.objectsByPageId) {
                pageIdToNumberMap[pageId] = parseInt(downloadManifest.objectsByPageId[pageId].page_img.page_number);
            }
        }

        return pageIdToNumberMap;
    }

    public getBookPageNumberToIdMap(bookId: string) {
        let pageNumberToIdMap = {};
        let downloadManifest = this.managedBookSet[bookId];

        if (downloadManifest != null) {

            for (let pageId in downloadManifest.objectsByPageId) {
                pageNumberToIdMap[downloadManifest.objectsByPageId[pageId].page_img.page_number] = parseInt(pageId);
            }
        }

        return pageNumberToIdMap;
    }

    private getDownloadManifest(legacyManifest, bookId, cdnURLSet) {

        let downloadManifest = {
            book_id: bookId,
            cdnURLs: cdnURLSet,
            book_format: legacyManifest.book_format,
            version_number: legacyManifest.version_number,
            objectsByPageId: {},
            teacher_layer: null
        };

        let legacyManifestPagesById: any = [];
        let legacyManifestOEDsByPageId: any = [];

        for (let page of legacyManifest.pages) {
            legacyManifestPagesById[page.page_id] = page;
            legacyManifestOEDsByPageId[page.page_id] = [];
        }

        if (legacyManifest.files != null) {
            for (let file of legacyManifest.files) {
                for (let pageId of file.page_id) {
                    let oed = this.getOEDObjectFromFileManifestInfo(bookId, pageId, file);
                    legacyManifestOEDsByPageId[pageId].push(oed);
                }
            }
        }

        let manifestObj = legacyManifest.manifest;
        for (let legacyBlock of legacyManifest.blocks) {
            for (let pageId of legacyBlock.page_ids) {
                pageId = parseInt(pageId);
                let oldPage = legacyManifestPagesById[pageId];

                if (oldPage != undefined) {

                    let pageImgObj: DownloadableObject = this.getPageDownloadableObjectFromFileManifestInfo(bookId, oldPage);

                    let pageInfo = {
                        page_id: pageId,
                        block_number: legacyBlock.block_number,
                        page_img: pageImgObj,
                        oeds: legacyManifestOEDsByPageId[pageId]
                    };

                    downloadManifest.objectsByPageId[pageId] = pageInfo;
                }
            }

            if (legacyManifest.teacher != null) {
                if (legacyManifest.teacher.length == 1) {
                    let teacherObj = legacyManifest.teacher[0];
                    downloadManifest.teacher_layer = this.getTeacherLayerObjFromFileManifestInfo(bookId, teacherObj);
                }
            }
        }

        return downloadManifest;
    }

    public async prepareBookForUpdate(bookId: string, oldManifest, newManifest): Promise<boolean> {

        this.bookStatusData[bookId] = 'atualizando';

        let preUpdateObjects = this.getOldObjectsByInvariantIds(bookId, oldManifest);

        let newResourceFiles = newManifest.files;
        let newPageFiles = newManifest.pages;
        let newLayerFiles = [];

        if (newManifest.hasOwnProperty("teacher")) {
            newLayerFiles = newManifest.teacher;
        }

        let newObjects = new Array<any>();

        for (let i = 0; i < newResourceFiles.length; i++) {
            let currResource = newResourceFiles[i];
            currResource["object_type"] = "F";
            newObjects.push(currResource);
        }
        for (let i = 0; i < newPageFiles.length; i++) {
            let currPage = newPageFiles[i];
            currPage["object_type"] = "P";
            newObjects.push(currPage);
        }

        for (let i = 0; i < newLayerFiles.length; i++) {
            let currLayer = newLayerFiles[i];
            currLayer["object_type"] = "T";
            newObjects.push(currLayer);
        }

        let updatedObjects = new Array<any>();
        for (let i = 0; i < newObjects.length; i++) {
            let currObj = newObjects[i];

            let uniqueObjIdentifier = null;

            let objType = currObj["object_type"];
            if (objType == 'F') {
                uniqueObjIdentifier = currObj["resource_filename"];
            } else if (objType == 'P') {
                uniqueObjIdentifier = currObj["page_id"];
            } else if (objType == 'T') {
                uniqueObjIdentifier = currObj["exported_filename"];
            }


            let oldObj = preUpdateObjects[uniqueObjIdentifier];

            if (oldObj != null) {

                let manifestHash = currObj["hash"];
                let oldObjHash = oldObj["object_id"];

                if (oldObjHash != manifestHash) {
                    oldObj.object_type = currObj.object_type;
                    updatedObjects.push(currObj);
                }
                else
                    // this object is referenced on the new manifest;
                    // so we must remove it from the "oldObjects" list since
                    // this list must only contain the "orphaned" ones in the end
                    delete preUpdateObjects[uniqueObjIdentifier];
            }
        }

        let orphanedObjects = new Array<any>();
        // unreferenced objects on the new manifest should also be marked
        // for removal by installer
        for (let key in preUpdateObjects) {

            orphanedObjects.push(preUpdateObjects[key]);
        }

        return this.updateDownloadManifest(bookId, orphanedObjects, updatedObjects);

    }

    private updateDownloadManifest(bookId, orphanedObjects, updatedObjects): Promise<boolean> {
        let downloadManifest = this.managedBookSet[bookId];

        return new Promise<boolean>(async (resolve, reject) => {
            if (downloadManifest != null) {
                await this.removeOrphanedObjects(downloadManifest, orphanedObjects);
                await this.updateObjects(bookId, downloadManifest, updatedObjects);

                this.resumeDownload(bookId);
                resolve(true);
            }
            else {
                resolve(false);
            }
        });
    }

    private updateObjects(bookId: string, downloadManifest, updatedObjects: Array<any>) {

        // pages must be threated first
        updatedObjects = updatedObjects.sort((obj1, obj2) => {
            if (obj1.object_type == 'P') {
                if (obj2.object_type != obj1.object_type) {
                    return -1;
                }
                else {
                    return 0;
                }
            }
            else if (obj2.object_type == 'P') {
                if (obj2.object_type == obj1.object_type) {
                    return 0;
                }
                else {
                    return 1;
                }
            }
            else {
                return 0;
            }
        });


        for (let obj of updatedObjects) {
            if (obj.object_type == 'F') {
                let pageIds = obj.page_id;
                if (pageIds != null) {
                    for (let pageId of pageIds) {
                        let pageOeds = downloadManifest.objectsByPageId[obj.page_id].oeds;
                        let foundSame = false;
                        if (pageOeds != null) {
                            for (let oedIndex in pageOeds) {

                                let oed = pageOeds[oedIndex];

                                if ((oed != null) && (oed.resource_filename == obj.resource_filename)) {
                                    pageOeds[oedIndex] = this.getOEDObjectFromFileManifestInfo(bookId, pageId, obj);
                                    foundSame = true;
                                }
                            }
                        }
                        else {
                            downloadManifest.objectsByPageId[obj.page_id].oeds = new Array<DownloadableObject>();
                        }
                        if (!foundSame) {
                            downloadManifest.objectsByPageId[obj.page_id].oeds.push(this.getOEDObjectFromFileManifestInfo(bookId, pageId, obj))
                        }
                    }
                }
            }
            else if (obj.object_type == 'P') {
                let pageDObj = this.getPageDownloadableObjectFromFileManifestInfo(bookId, obj);
                if (downloadManifest.objectsByPageId[obj.page_id] == null) {
                    downloadManifest.objectsByPageId[obj.page_id] = {
                        page_img: pageDObj,
                        oeds: []
                    }
                }
                else {
                    downloadManifest.objectsByPageId[obj.page_id].page_img = pageDObj;
                }
            }
            else if (obj.object_type == 'T') {
                downloadManifest.teacher_layer = this.getTeacherLayerObjFromFileManifestInfo(bookId, obj);
            }
        }
    }

    private async removeOrphanedObjects(downloadManifest, orphanedObjects) {
        // orphaned objects should be removed from the download manifest
        for (let obj of orphanedObjects) {
            if (obj.object_type == 'F') {
                let pageIds = obj.page_id;
                if (pageIds != null) {
                    for (let pageId of pageIds) {
                        let pageOeds = downloadManifest.objectsByPageId[obj.page_id].oeds;
                        if (pageOeds != null) {
                            for (let oedIndex in pageOeds) {
                                let oed = pageOeds[oedIndex];
                                if ((oed != null) && (oed.resource_filename == obj.resource_filename)) {
                                    await this.installer.uninstallOED(oed);
                                    pageOeds.splice(oedIndex, 1);
                                }
                            }
                        }
                    }
                }
            }
            else if (obj.object_type == 'P') {
                let pageContainer = downloadManifest.objectsByPageId[obj.page_id].page_img;
                await this.installer.uninstallPage(pageContainer);
                delete downloadManifest.objectsByPageId[obj.page_id];

            }
            else if (obj.object_type == 'T') {
                await this.installer.uninstallTeacherLayer(downloadManifest.teacher_layer);
                downloadManifest.teacher_layer = null;

            }
        }
    }

    private getOldObjectsByInvariantIds(bookId: string, oldManifest) {
        let objectsByInvariantId = {};

        let pageObjects: Array<any> = oldManifest.pages;
        if (pageObjects != null) {
            for (let pageElement of pageObjects) {

                // for pages, the invariant id is pageId
                pageElement.object_id = pageElement.hash;
                objectsByInvariantId[pageElement.page_id] = pageElement;
            }
        }

        if (oldManifest.files != null) {
            for (let fileElement of oldManifest.files) {
                fileElement.object_id = fileElement.hash;

                objectsByInvariantId[fileElement.resource_filename] = fileElement;
            }
        }

        if (oldManifest.teacher != null) {
            if (oldManifest.teacher.length == 1) {
                let teacherObj = oldManifest.teacher[0];
                teacherObj.object_id = teacherObj.hash;

                objectsByInvariantId[teacherObj.exported_filename] = teacherObj;
            }
        }

        return objectsByInvariantId;
    }

    private getOEDObjectFromFileManifestInfo(bookId: string, pageId: number, oedElement): DownloadableObject {

        let oed: DownloadableObject = {
            resource_filename: oedElement.resource_filename,
            compressed_size: parseInt(oedElement.exported_filesize),
            exported_filename: oedElement.exported_filename,
            book_version: oedElement.book_version,
            exported_filesize_uncompressed: parseInt(oedElement.exported_filesize_uncompressed),
            compressed_downloaded_size: 0,
            obj_type: 'O',
            book_id: bookId,
            page_id: pageId,
            status: "parado"
        };

        return oed;
    }

    private getPageDownloadableObjectFromFileManifestInfo(bookId: string, pageElement): DownloadableObject {
        let pageImgObj: DownloadableObject = {
            page_id: pageElement.page_id,
            book_id: bookId,
            obj_type: 'P',
            compressed_size: parseInt(pageElement.exported_filesize),
            exported_filename: pageElement.exported_filename,
            book_version: pageElement.book_version,
            exported_filesize_uncompressed:
                parseInt(pageElement.exported_filesize_uncompressed),
            compressed_downloaded_size: 0,
            page_number: parseInt(pageElement.page_number),
            status: "parado"
        };
        return pageImgObj;
    }

    private getTeacherLayerObjFromFileManifestInfo(bookId, teacherElement) {
        let teacherObj: DownloadableObject = {
            book_id: bookId,
            book_version: teacherElement.book_version,
            exported_filename: teacherElement.exported_filename,
            exported_filesize_uncompressed: parseInt(teacherElement.exported_filesize_uncompressed),
            compressed_size: parseInt(teacherElement.exported_filesize),
            compressed_downloaded_size: 0,
            obj_type: 'T',
            status: 'parado'
        };

        return teacherObj;
    }
}

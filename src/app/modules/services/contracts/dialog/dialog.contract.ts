export interface DialogContract {
  onConfirm(message: string, title?: string, buttons?: Array<string>): Promise<boolean>

  onPrompt(message: string,
           defaultText?: string,
           inputType?: string,
           inputValue?: string,
           inputAttrs?: {},
           selectOptions?: {},
           title?: string): Promise<any>

  onAlert(description: string, title: string): Promise<any>
}

/**
 * @Layout
 * - Mobile: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html#where-to-store-files
 * - OS: https://electronjs.org/docs/api/remote
 */
export enum LayoutFileSystemEnum {
    MOBILE_APPLICATION_STORAGE_DIRECTORY,
    MOBILE_APPLICATION_DIRECTORY,
    MOBILE_DOCUMENTS,
    MOBILE_DATA_DIRECTORY,
    MOBILE_SYNCED_DATA_DIRECTORY,
    MOBILE_CACHE_DIRECTORY,
    MOBILE_TEMP_DIRECTORY,
    MOBILE_EXTERNAL_ROOT_DIRECTORY,
    MOBILE_EXTERNAL_APPLICATION_STORAGE_DIRECTORY,
    MOBILE_EXTERNAL_CACHE_DIRECTORY,
    MOBILE_EXTERNAL_DATA_DIRECTORY,
    MOBILE_SHARED_DIRECTORY,
    NO_ROOT_PATH,
    ROOT_PATH
}
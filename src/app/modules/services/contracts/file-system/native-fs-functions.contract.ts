import { LayoutFileSystemEnum } from "./layout-file-system.enum";

export interface NativeFSFunctionsContract {
    nativeOpen?(path: string|Buffer|URL, flags: string|number, mode: number, callback: Function);
    nativeWrite?(fd: number, buffer: Buffer|Uint8Array, offset: number, length: number, position: number, callback: Function);
    nativeClose?(fd: number, callback: Function);
    readFileSync?(path: string, layout?: LayoutFileSystemEnum): Buffer|string;
}

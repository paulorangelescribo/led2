export interface ReadFileOptionsContract {
    toJSON(successHandler: Function, errorHandler: Function): void
    toString(successHandler: Function, errorHandler: Function): void
    get(): Promise<string|Buffer>
}

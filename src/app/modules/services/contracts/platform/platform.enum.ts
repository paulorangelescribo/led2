export enum PlatformEnum {
    MOBILE,
    DESKTOP,
    WEB,
    IOS,
    ANDROID,
    WINDOWS,
    MACOS,
    LINUX
}
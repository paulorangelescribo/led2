export class Platform {
    static readonly TYPE: string[] = ['mobile', 'desktop', 'web', 'ios', 'android', 'win32', 'darwin', 'linux'];
}
export interface ZipContract {
  unzip(options: { from: string, to: string }, successHandler: Function, errorHandler: Function)
}

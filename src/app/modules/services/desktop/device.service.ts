import { DeviceContract } from '../contracts/device/device.contract';
import { Injectable } from '@angular/core';
declare var window;

@Injectable()
export class DeviceService implements DeviceContract {

    private os: any;
    readonly platformId: string;
    constructor() {
        this.os = window.require('os');
        this.platformId = this.os.platform();
    }

    prepareDeviceInfoRequest() {
        return {
            'device_name': this.os.hostname(),
            'device_info': [{
                'model': 'desktop',
                'os_name': this.os.platform(),
                'os_version': this.os.release(),
                'screen_height': window.screen.height * window.devicePixelRatio,
                'screen_width': window.screen.width * window.devicePixelRatio
            }]
        };
    }
}

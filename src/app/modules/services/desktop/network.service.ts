import { NetworkContract } from '../contracts/network/network.contract';
import { Injectable } from '@angular/core';

@Injectable()
export class NetworkService implements NetworkContract {
    isLimitedConnection() {
        return false;
    }

    isConnected() {
        return navigator.onLine;
    }
}

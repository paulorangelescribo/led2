import { ZipContract } from '../contracts/zip/zip.contract';
import { Injectable, Inject } from '@angular/core';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
declare var window;

@Injectable()
export class ZipService implements ZipContract {
    private counter: number;
    constructor(@Inject('FileSystemService') private fileSystem: LedFileSystemContract) {}

    unzip(options: { from: string, to: string }, successHandler: any, errorHandler: any) {
        this.counter = 0;
        const JSZip = window.require("jszip");
        const zip = new JSZip();
        const from = this.fileSystem.readFileSync(options.from);
        zip.loadAsync(from).then(contents => {


            let allFilenames = Object.keys(contents.files);

            let loopCall: Function = (filename) => {

                let requestPath = options.to + filename.replace(/\\/g, "/");

                if (zip.files[filename].dir) {

                    if (this.counter == (allFilenames.length - 1)) {
                        successHandler(true);
                    }
                    else {
                        this.counter++;
                        loopCall(allFilenames[this.counter])
                    }

                } else {

                    zip.files[filename].async('uint8array').then((content) => {

                        this.ensureParentDirExistence(requestPath).then(() => {
                            const request = this.fileSystem.request(requestPath);


                            request.writeFile(content, () => {

                                if (this.counter == (allFilenames.length - 1)) {

                                    successHandler(true);
                                }
                                else {
                                    this.counter++;
                                    loopCall(allFilenames[this.counter])
                                }

                            }, errorHandler);
                        }).catch(errorHandler);
                    }).catch((error) => {
                        let errorStr = "ZipService -  unzip - erro acessando " + filename + ": (" + JSON.stringify(error) + ")";
                        errorHandler(errorStr);
                    });
                }
            }

            loopCall(allFilenames[0]);

        }).catch(errorHandler);
    }

    private ensureParentDirExistence(filePath: string): Promise<any> {
        let lastSlashIndex = filePath.lastIndexOf("/");
        if (lastSlashIndex != (filePath.length - 1)) {
            filePath = filePath.substring(0, lastSlashIndex);
        }
        return this.fileSystem.request(filePath).createDir();
    }
}

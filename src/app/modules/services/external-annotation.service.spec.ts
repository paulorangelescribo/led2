import { TestBed, inject } from '@angular/core/testing';

import { ExternalAnnotationService } from './external-annotation.service';

describe('ExternalAnnotationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExternalAnnotationService]
    });
  });

  it('should be created', inject([ExternalAnnotationService], (service: ExternalAnnotationService) => {
    expect(service).toBeTruthy();
  }));
});

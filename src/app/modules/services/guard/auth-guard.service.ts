import { Injectable, Inject } from '@angular/core';
import { UserLoginFieldsContract } from "../../../contracts/user-login-fields.contract";
import { environment } from "../../../../environments/environment";
import { DeviceGuardService } from "./device-guard.service";
import { HttpService } from "../http/http.service";
import { HttpScaoResponse } from "../http/util/http-scao-response";
import { AuthStorageService } from "./auth-storage.service";
import { GuardConstants } from "./consts/guard-constants";
import { UserLoggedFieldsContract } from "../../../contracts/user-logged-fields.contract";
import { LegacyBookContract } from "../../../contracts/legacy-book.contract";
import { SessionGuardContract } from "./session-guard-contract";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route, CanLoad } from "@angular/router";
import { BookDao } from "../../daos/book.dao";
import { DeviceContract } from '../contracts/device/device.contract';
import { NetworkContract } from '../contracts/network/network.contract';
import { UserMemoryFieldsContract } from "../../../contracts/user-memory-fields.contract";

@Injectable()
export class AuthGuardService implements CanActivate, CanLoad {
    private Instance: AuthGuardService;
    private supportedBookFormats: string[] = environment.supported_book_formats;

    constructor(private deviceGuard: DeviceGuardService,
        private http: HttpService,
        @Inject('DeviceService') private deviceService: DeviceContract,
        @Inject('NetworkService') private network: NetworkContract,
        private storage: AuthStorageService,
        private router: Router,
        private book: BookDao) {
        this.book.AuthGuard = this;
        this.storage.book = this.book;
    }

    getStorage(): AuthStorageService {
        return this.storage;
    }

    getDevice(): DeviceGuardService {
        return this.deviceGuard;
    }

    user(): UserLoggedFieldsContract {
        if (this.storage.getGuard()) return this.storage.getGuard().user;
    }

    books(): LegacyBookContract[] {
        if (this.storage.getGuard()) return this.storage.getGuard().books;
    }

    has(field: string) {
        return this.storage.currentSession['memory'] && this.storage.currentSession['memory'].hasOwnProperty(field);
    }

    get(field: string) {
        return this.storage.currentSession['memory'][field];
    }

    isRemember(): boolean {
        let hasSession = false;
        if (typeof this.storage.currentSession != 'undefined' && this.storage.currentSession != null) {
            hasSession = (Object.keys(this.storage.currentSession).length > 0);
        }

        const isRemember = (hasSession && this.storage.currentSession.remember == true);
        return isRemember;
    }

    isLogged(): boolean {
        let isLogged = false;
        if (typeof this.storage.currentSession != 'undefined' && this.storage.currentSession != null) {
            if (Object.keys(this.storage.currentSession).length > 0) isLogged = true;
        }
        return isLogged;
    }

    logout() {
        return this.storage.destroy();
    }

    private async check(route: string) {
        await this.storage.loadCurrentSession();
        if (!this.isLogged()) {
            this.router.navigate(['login']);
            return false;
        }

        return true;
    }

    async canLoad(route: Route) {
        let url = `/${route.path}`;

        const canLoad = await this.check(url);

        return canLoad;
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string = state.url;

        const canActivate = await this.check(url);

        return canActivate;
    }

    async auth(params: UserLoginFieldsContract) {
        let response = null;

        if (!this.network.isConnected())
            response = this.storage.localAuth(params);
        else
            response = await this.resolveEndpointAuth(params);

        return response;
    }

    set currentSession(value: any) {
        this.storage.currentSession = value;
    }

    get currentSession() {
        return this.storage.currentSession;
    }

    private async resolveEndpointAuth(params: UserLoginFieldsContract) {
        this.prepareRequests(params);

        params.device_id = await this.loadDeviceId();

        const data = JSON.stringify(params);
        const URL = this.http.getURL('scao', 'ws007');
        const response: HttpScaoResponse = await this.http.makeRequest(URL, data);

        if (response.hasErrors()) throw new Error(response.getErrorMessage());

        response.appendField('remember', params.remember);
        response.appendField('password', params.password);

        this.storage.setGuard(GuardConstants.DEFAULT_GUARD, {});
        this.storage.setUser(response);
        this.storage.setUserLiteraryWorks(response);

        await this.storage.save();

        return response;
    }

    private async loadDeviceId() {
        return this.deviceGuard.getUniqueIdentifier();
    }

    private prepareRequests(params) {
        Object.assign(params, { supported_book_formats: this.supportedBookFormats });
        Object.assign(params, this.deviceService.prepareDeviceInfoRequest());
        this.deviceGuard.prepareUuidRequest(params);
    }

}

import { UserLoggedFieldsContract } from "../../../contracts/user-logged-fields.contract";
import { LegacyBookContract } from "../../../contracts/legacy-book.contract";

export interface SessionGuardContract {
    user: UserLoggedFieldsContract;
    books?: LegacyBookContract[];
}

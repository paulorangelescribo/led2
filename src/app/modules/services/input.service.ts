import { Injectable, Inject } from '@angular/core';
import { BookDao } from '../daos/book.dao';
import { PlatformContract } from './contracts/platform/platform.contract';
import { LedFileSystemContract } from './contracts/file-system/led-file-system.contract';
@Injectable()
export class InputService {
    private stdIN: string;
    private jsonData: any;
    private books: Array<any> = [];

    constructor(@Inject('PlatformService') private platform: PlatformContract,
        @Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        private book: BookDao) {
        this.platform.ready().then(() => {
            this.book.fetchData().then(bookList => this.books = bookList).catch(error => console.log(error));
        });
    }

    setData(data: string) {
        this.stdIN = data.replace('alfaconbooks://viewBook?data=', '');
        this.jsonData = JSON.parse(decodeURIComponent(this.stdIN));
    }

    toString() {
        return this.stdIN;
    }

    toObject() {
        return this.jsonData;
    }

    filterData(): Promise<any> {
        return new Promise((resolve, reject) => {

            const bookArray = this.books.filter(book => {
                // tslint:disable-next-line:radix
                return parseInt(book.book_publisher_id) === this.jsonData.book_publisher_id;
            });

            if (bookArray.length > 0) {
                resolve(bookArray[0]);
            } else {
                reject(
                    'Livro não encontrado. publisher_id = ' +
                    this.jsonData.book_publisher_id
                );
            }

        });
    }
}

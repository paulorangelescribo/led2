import { DownloadContract } from '../contracts/download/download.contract';
import { Loadingstatus } from '../contracts/download/loadingstatus';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { Injectable, Inject } from '@angular/core';

@Injectable()
export class DownloadService implements DownloadContract {
  fileTransfer: FileTransfer;
  loadingStatus: Loadingstatus;
  constructor(@Inject('PlatformService') private platform: PlatformContract) {
    this.platform.ready().then(() => {
      this.fileTransfer = new FileTransfer();
    });

  }

  download(source: string, target: string, successCallback?: (fileEntry: FileEntry) => void,
           errorCallback?: (error: FileTransferError) => void, trustAllHosts?: boolean,
           options?: FileDownloadOptions, xorKey?: string): void {
    return this.fileTransfer.download(source, target, successCallback, errorCallback, trustAllHosts, options, xorKey);
  }
  showProgress(received, total, progressEvent) {
    if (progressEvent.lengthComputable) {
      this.loadingStatus.setPercentage(received / total);
    } else {
      this.loadingStatus.increment();
    }
    return this.loadingStatus.getStatus();
  }

}

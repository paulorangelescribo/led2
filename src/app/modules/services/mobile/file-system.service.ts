import { WebWorkerService } from "angular2-web-worker";
import { LedFileSystemContract } from "../contracts/file-system/led-file-system.contract";
import { VaultGeneratorService } from "../vault/vault-generator.service";
import { LayoutFileSystemEnum } from "../contracts/file-system/layout-file-system.enum";
import { DiskUsageContract } from "../contracts/file-system/disk-usage.contract";
import { Injectable, Inject } from "@angular/core";
import { PlatformContract } from "../contracts/platform/platform.contract";
declare var DiskSpacePlugin;
declare var navigator;
declare var self;

@Injectable()
export class FileSystemService implements LedFileSystemContract {
    private requestPath: string;
    private layout;
    private reader;
    private path;
    private isPlatformReady: boolean = false;
    private worker: WebWorkerService;
    protected interactivePromise;
    static PID: number = 0;
    private process: Array<FileSystemService> = [];
    private currentProccess: FileSystemService;

    constructor(private vaultGenerator: VaultGeneratorService,
        @Inject("PlatformService") private platform: PlatformContract) {}

    request(path?: string, layout: LayoutFileSystemEnum = LayoutFileSystemEnum.ROOT_PATH) {
        const PID = ++FileSystemService.PID;
        this.process[PID] = this.currentProccess = new FileSystemService(this.vaultGenerator, this.platform);

        if (layout === LayoutFileSystemEnum.ROOT_PATH) {
            this.currentProccess.requestPath = this.getDirectory();
        } else if (layout === LayoutFileSystemEnum.NO_ROOT_PATH) {
            this.currentProccess.requestPath = path;
        } else {
            this.currentProccess.requestPath = "";
        }

        this.currentProccess.path = path;
        this.currentProccess.layout = layout;
        this.currentProccess.worker = new WebWorkerService();
        return this.currentProccess;
    }

    exists(path: string, handler: Function): void {
        let result: boolean = false;
        (async () => {
            await this.platform.ready();
            window.resolveLocalFileSystemURL(
                this.getDirectory() + path,
                (entry: FileEntry | DirectoryEntry) => {
                    if (entry.isDirectory) result = true;
                    if (entry.isFile) result = true;
                    return handler(result);
                },
                error => {
                    console.log("method exists() " + JSON.stringify(error));
                    return handler(false);
                }
            );
        })();
    }

    readDir(options?: string | object): Promise<string[] | Buffer[]> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            window.resolveLocalFileSystemURL(this.getRequestPath(), (dirEntry: DirectoryEntry) => {
                if (!dirEntry.isDirectory) return reject("Request path is not a directory: " + this.getRequestPath());

                const readDir = dirEntry.createReader();

                readDir.readEntries((entries: Entry[]) => {
                    const entriesPATH = [];

                    entries.forEach(entry => {
                        entriesPATH.push(entry.nativeURL);
                    });

                    return resolve(entriesPATH);
                }, reject);
            });
        });
    }

    readFile() {
        this.interactivePromise = new Promise(async (resolve, reject) => {
            this.getFileEntry().then((entry: FileEntry) => {
                const reader = new FileReader();
                entry.file(file => {

                    if (file.size === 0) return reject(FileError.NOT_FOUND_ERR);

                    reader.onabort = (event) => {
                        console.log(reader.abort);
                    };

                    reader.onloadend = () => {
                        return resolve(new Uint8Array(reader.result));
                    };

                    reader.onerror = () => {
                        return reject(reader.error);
                    };

                    reader.readAsArrayBuffer(file);
                }, error => {
                    return reject(error);
                });
            });
        });

        return this;
    }

    writeFile(data: string | Buffer | Uint8Array, successHandler: Function, errorHandler: Function) {
        this.getFileEntry()
            .then((result: FileEntry) => {

                result.createWriter(
                    writer => {
                        try {

                            const blobJSON = new Blob([data], { type: "application/json" });

                            if (blobJSON.size === 0) return errorHandler(FileError.INVALID_MODIFICATION_ERR);

                            writer.onwrite = (event) => {
                                console.log(event);
                            };
                            writer.onwriteend = event => {
                                return successHandler(event);
                            };

                            writer.onerror = event => {
                                return errorHandler(writer.error);
                            };

                            writer.write(blobJSON);
                        } catch (error) {
                            console.log(error);
                        }
                    },
                    (error: FileError) => {
                        return errorHandler(error);
                    }
                );
            })
            .catch((error: FileError) => {
                console.log(error);
                return errorHandler(error);
            });

        return this;
    }

    rename(oldPath: string, newPath: string): void {
        //
    }

    createDir() {
        const _self = this;
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            const parts = this.path.split("/");
            let acc = "";
            parts.pop();

            window.resolveLocalFileSystemURL(this.getDirectory(), (rootDirEntry: DirectoryEntry) => {
                make(parts.shift(), rootDirEntry);

                function make(dir, dirEntry) {
                    if (dir === undefined) {
                        return resolve(_self.path);
                    } else {
                        rootDirEntry.getDirectory(
                            acc + dir,
                            { create: true },
                            dirEntry => {
                                acc += dir + "/";
                                make(parts.shift(), dirEntry);
                            },
                            reject
                        );
                    }
                }
            });
        });
    }

    delete(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            const PATH = this.path ? this.getRequestPath() + this.path : this.getRequestPath();
            window.resolveLocalFileSystemURL(
                PATH,
                (entry: FileEntry | DirectoryEntry) => {
                    const remover = {
                        file: (entry: FileEntry, handlerResolve: any, rejectResolve: any) => {
                            entry.remove(handlerResolve, rejectResolve);
                        },
                        directory: (entry: DirectoryEntry, handlerResolve: any, rejectResolve: any) => {
                            entry.removeRecursively(handlerResolve, rejectResolve);
                        }
                    };

                    if (entry.isFile) remover.file(<FileEntry>entry, resolve, reject);
                    else if (entry.isDirectory) remover.directory(<DirectoryEntry>entry, resolve, reject);
                    else reject("The request path is not a directory and is also not a file");
                },
                reject
            );
        });
    }

    getDirectory(): string {
        let directory = "cdvfile://localhost/";
        try {
            if (cordova.platformId.toLowerCase() == "ios") directory = cordova.file.dataDirectory;
            else if (cordova.platformId.toLowerCase() == "android") directory = cordova.file.externalDataDirectory;
        } catch (error) {
            console.log("getDirectoryError", error);
            if (cordova.platformId.toLowerCase() == "ios") directory = "cdvfile://localhost/library-nosync/";
            else if (cordova.platformId.toLowerCase() == "android") directory = "cdvfile://localhost/files-external/";
        }

        return directory;
    }

    getRequestPath(): string {
        return this.requestPath;
    }

    getDiskUsage(): Promise<DiskUsageContract> {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            DiskSpacePlugin.info(
                { location: 1 },
                response => {
                    let data: DiskUsageContract = {
                        app: response.app,
                        free: response.free
                    };

                    resolve(data);
                },
                reject
            );
        });
    }

    openFileExternally(filePath: string, mimeType: string): Promise<any> {
        return new Promise<any>(async (resolve, reject) => {
            await this.platform.ready();
            const open = cordova.plugins["disusered"]["open"];
            open(filePath);
            // cordova.plugins["fileOpener2"].open(filePath, mimeType, {
            //     error: error => {
            //         let errorStr = "Erro chamando fileOpener2: " + JSON.stringify(error);
            //         reject(error);
            //     },
            //     success: () => {
            //         resolve();
            //     }
            // });
        });
    }

    toJSON(successHandler: Function, errorHandler: Function): void {
        this.interactivePromise
            .then(async (response: Uint8Array) => {
                await this.platform.ready();
                const blobJSON = new Blob([response], { type: "application/json" });

                const Reader = new FileReader();

                Reader.onloadend = () => {
                    if (Reader.result && Reader.result !== "") return successHandler(JSON.parse(Reader.result));
                    else return errorHandler(null);
                };

                Reader.readAsText(blobJSON);
            })
            .catch(errorHandler);
    }

    get(): Promise<string | Buffer> {
        return this.interactivePromise;
    }

    toString(successHandler: Function, errorHandler: Function): void {
        this.interactivePromise.then(async response => {
            await this.platform.ready();
            const blobJSON = new Blob([response], { type: "text/plain" });

            const Reader = new FileReader();

            Reader.onloadend = () => {
                if (Reader.result && Reader.result !== "") return successHandler(Reader.result);
                else return errorHandler(null);
            };

            Reader.readAsText(blobJSON);
        });
    }

    private getFileEntry() {
        return new Promise(async (resolve, reject) => {
            await this.platform.ready();
            if (this.getRequestPath() === null || this.getRequestPath() === undefined) return;

            window.resolveLocalFileSystemURL(
                this.getRequestPath(),
                (entry: DirectoryEntry) => {
                    console.log(entry.toInternalURL());
                    console.log(entry.nativeURL);
                    if (entry.isDirectory) {
                        entry.getFile(
                            this.path,
                            { create: true },
                            fileEntry => {
                                console.log(fileEntry.toInternalURL());
                                console.log(fileEntry.nativeURL);
                                return resolve(fileEntry);
                            },
                            error => {
                                return reject(error);
                            }
                        );
                    } else if (entry.isDirectory && !this.path) return reject("NO PATH: getFileEntry method");
                    else return resolve(entry);
                },
                (error: FileError) => {
                    return reject(error);
                }
            );
        });
    }

    openLinkExternally(urlPath: string) {
        if (typeof cordova !== "undefined") {
            if (cordova.platformId == "android")
                // Android
                navigator.app.loadUrl(urlPath, { openExternal: true });
            else window.open(urlPath, "_system"); // iOS
        } else {
            let shell = self.require("electron").shell; // Electron
            shell.openExternal(urlPath);
        }
        return;
    }
}

import { NetworkContract } from '../contracts/network/network.contract';
import { NetworkEnum } from '../contracts/network/network.enum';
import { Injectable } from '@angular/core';
declare const navigator;

@Injectable()
export class NetworkService implements NetworkContract {
    isLimitedConnection(): boolean {
        const connectionType = navigator.connection.type.toLowerCase();
        const is2GConnection = connectionType.indexOf(NetworkEnum.CELL_2G) !== -1;
        const is3GConnection = connectionType.indexOf(NetworkEnum.CELL_3G) !== -1;
        const is4GConnection = connectionType.indexOf(NetworkEnum.CELL_4G) !== -1;
        return (is2GConnection || is3GConnection || is4GConnection);
    }

    isConnected(): boolean {
        const connectionType = navigator.connection.type.toLowerCase();
        return connectionType !== NetworkEnum.NONE;
    }
}

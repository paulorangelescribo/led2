import { EPubItemContract } from './e-pub-item.contract';
export interface BookReaderCollectionContract {
    pages: EPubItemContract[];
    images?: EPubItemContract[];
    styles?: EPubItemContract[];
    fonts?: EPubItemContract[];
    pls?: EPubItemContract[];
}

import { EPubSpineContract } from './e-pub-spine.contract';
import { EPubItemContract } from './e-pub-item.contract';
import { EPubMetadataContract } from './e-pub-metadata.contract';
export interface PackageContract {
    metadata: EPubMetadataContract;
    manifest: Array<EPubItemContract>;
    spine: Array<EPubSpineContract>;
}

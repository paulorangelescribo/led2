import { BookReaderCollectionContract } from './book-reader-collection.contract';
import {EPubSpineContract} from "./e-pub-spine.contract";
import {EPubItemContract} from "./e-pub-item.contract";
export interface ReaderContract {
    readonly booksPATH;
    init(bookId: string, handler: any): void
    getBookId(): string
    getBasePath(path?: string): string
    getFile(fileName: any, fileHandler: any, errorHandler?: any): void
    getCollection(): BookReaderCollectionContract
    findPage?(page: string, bookList: Array<any>): { spineItem: EPubSpineContract, summary: EPubItemContract, anchor: string, pageNumber: number}
    destroy(): void
    getInstalledManifest()
}

import { BookEvent } from '../../events/book/book.event';
import { PageReaderService } from './page-reader.service';
import { EPubItemContract } from './contracts/e-pub-item.contract';
import { EPubTextformatThemeTypeEnum } from '../../reader/components/e-pub-textformat/e-pub-textformat-themetypes.enum';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { PlatformEnum } from '../contracts/platform/platform.enum';
declare var MobileAccessibility;
declare var window;

export class EpubDocumentService {
    private isLoad: boolean = false;
    private fileBlob: Blob;
    private epubWrap: any;
    private epubContainer: any;
    private HammerJS: HammerManager;
    private countSwipes: number = 0;
    private leftPosition: number = 0;
    private pageNumber: number = 0;
    private bookEvent: BookEvent;
    private PageReader: PageReaderService;
    private totalColumns: number = 0;
    private countTapped: number = 0;
    private tapped: boolean = false;
    private _style: any;
    private oldStyles: any = {};
    private dimensions: { width: any, height: any } = { width: null, height: null };
    private _styleChanged: boolean = false;
    private directory: string;
    private sizeOf: any;
    private platform: PlatformContract;

    private styleAsterisk: string[] = [];
    private styleBody: string[] = [];

    constructor(bytesArray: Array<any>, directory: string) {
        this.directory = directory;
        this.fileBlob = new Blob([bytesArray], { type: 'text/xhtml' });
        this.isLoad = true;
        this.leftPosition = 0;
        this.countSwipes = 0;
        this.pageNumber = 0;
        this._style = {};
    }

    resetPosition() {
        this.leftPosition = 0;
    }

    addTapEventListener(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (this.isLoad) {
                this.updateEpubContainer();
                this.epubContainer.body.removeEventListener('tap', () => {
                });
                this.epubContainer.body.addEventListener('tap', (event: Event) => {
                    const scrollWidth = this.epubContainer.body.scrollWidth + '';
                    const scrollHeight = this.epubContainer.body.scrollHeight;
                    this.updateEpubContainer(scrollWidth, scrollHeight);
                    resolve();
                }, true);
            }
        })
    }

    addDoubleTapEventListener(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (this.isLoad) {

                this.HammerJS = new Hammer(this.epubContainer.body, {
                    domEvents: true,
                    touchAction: 'pan-y'
                });
                this.doubletap(resolve);
            }
        })
    }


    addSwipeEventListener(): Promise<number> {

        return new Promise((resolve, reject) => {
            if (this.isLoad) {
                this.HammerJS = new Hammer(this.epubContainer.body, {
                    domEvents: true,
                    touchAction: 'pan-y'
                });

                this.HammerJS.get('swipe').set({ enable: true });

                this.swipeLeft(resolve);
                this.swipeRight(resolve);
            }
        });
    }

    addSummaryEventListener(anchor?: string, platform?: PlatformContract) {
        return new Promise((resolve, reject) => {

            if (platform.is(PlatformEnum.ANDROID)) {
                const epubWrap: any = $('#epub-wrap').get(0)['contentDocument'];

                $('a', epubWrap).each(function () {
                    $(this).click(resolve);
                });

            } else {

                const DOMIndexed = this.indexDOM(this.epubContainer);

                if (DOMIndexed.hasOwnProperty('A') && DOMIndexed.A.length) {
                    DOMIndexed.A.forEach(link => {
                        if (link.getAttribute('href') !== "") {
                            link.addEventListener("click", resolve, false);
                        }
                    });
                }
            }

            if (anchor) {
                setTimeout(() => {
                    this.epubContainer.location.hash = anchor.replace('#', '');
                }, 500);
            }

        });
    }

    setStyle(style: any) {
        this._style = style;
        this._styleChanged = true;
        // tslint:disable-next-line:forin
        const theme = style['theme'];
        if (this._styleChanged) {
            this.linkContrast('reset');
            this.clearInLineStyle();
            this.forceReplacementStyle();
        }

        if (theme === EPubTextformatThemeTypeEnum.ALT0_CONTRASTE) {
            this.linkContrast();
        }
        if (theme === EPubTextformatThemeTypeEnum.SEPIA) {
            this.replicateToChildren(this.epubContainer.getElementById('epub').children);

        }
        if (theme === EPubTextformatThemeTypeEnum.PADRAO) {
            this.linkContrast('reset');
            this.replicateToChildren(this.epubContainer.getElementById('epub').children, true);
            this._styleChanged = false;
        }
    }

    generateDocument(pageReader: PageReaderService): Promise<string> {
        return new Promise((resolve, reject) => {
            if (this.isLoad) {
                this.PageReader = pageReader;
                this.pageNumber = pageReader.getPageNumber();
                const readerCollection = pageReader.reader.getCollection();
                const styles: EPubItemContract[] = readerCollection.styles;

                const cssUri = this.directory + pageReader.reader.getBasePath(styles[0].href);
                const reader = new FileReader();

                reader.onloadend = () => {
                    const documentHTML = new DOMParser().parseFromString(
                        reader.result,
                        'application/xhtml+xml'
                    );

                    const imgs = documentHTML.body.getElementsByTagName('img');

                    for (let i = 0; i < imgs.length; i++) {

                        const selectedImage = imgs[i];

                        const sourcePATH = selectedImage.getAttribute('src').replace('../', '');
                        let path = this.directory + this.PageReader.reader.getBasePath(sourcePATH);
                        if (this.directory.indexOf('file://') === -1) {
                            path = 'file://' + path;
                        }

                        const dimensions = this.getSizeOf(path, selectedImage);
                        selectedImage.style['max-width'] = dimensions.width + 'px';
                        selectedImage.setAttribute('src', path);
                        selectedImage.setAttribute('width', '100%');
                        selectedImage.setAttribute('draggable', 'false');
                        if (selectedImage.parentElement) {
                            selectedImage.parentElement.setAttribute('drragable', 'false');
                        }

                    }

                    this.isVoiceOverRunning(documentHTML, cssUri, (div) => {
                        setTimeout(() => {
                            const arrayBytes = new Buffer(div.outerHTML);
                            return resolve(pageReader.getPage(arrayBytes, { type: 'text/html' }));
                        }, 125);
                    });

                };

                reader.readAsText(this.fileBlob);

            } else {
                return reject('Epub not loaded.');
            }
        });
    }

    updateEpubContainer(width?: string, height?: number) {

        this.dimensions.width = window.outerWidth;
        if (width) {
            this.dimensions.width = width;
        }

        if (height) {
            this.dimensions.height = height;
        }

        this.epubWrap = document.getElementById('epub-wrap');

        this.epubWrap.style.maxWidth = this.dimensions.width + 'px';

        this.epubContainer = this.epubWrap.contentDocument;
        const EPub = this.epubContainer.getElementById('epub');
        EPub.style['webkitColumnWidth'] = EPub.style['columnWidth'] = this.getMinWidth(EPub) + 'px';

        if (this.platform.is(PlatformEnum.MOBILE)) {
            const actionBarHeight = document.getElementById('epubActionbarReader').clientHeight;
            EPub.style.height = (this.dimensions.height - actionBarHeight) + 'px';
        } else if (this.platform.is(PlatformEnum.DESKTOP)) {
            EPub.style.height = this.dimensions.height + 'px';
        }

        // tslint:disable-next-line:max-line-length
        this.setTotalColumns(EPub);
    }

    /**
     * Made in Manga
     * @param e
     * @param o
     */
    indexDOM(e, o?: any) {
        e = e || document.body;
        o = o || {};
        o['$' + e.id] = e;
        (o[e.tagName] = o[e.tagName] || []).push(e);
        (e.className + '').split(/\s+/).forEach(p => (o[p] = o[p] || []).push(e));
        for (let i = 0, l = e.children.length; i < l; ++i) {
            this.indexDOM(e.children[i], o);
        }

        return o;
    }

    createDOM(tag?, props?, children?, parent?) {
        tag = props = children = parent = null;
        for (let i = 0; i < arguments.length; ++i) {
            if (typeof arguments[i] == 'string') {
                tag = arguments[i];
            }
            else if (typeof arguments[i] == 'object') {
                if (Array.isArray(arguments[i])) {
                    children = arguments[i];
                }
                else if (arguments[i] instanceof Element) {
                    parent = arguments[i];
                }
                else if (arguments[i]) {
                    props = arguments[i];
                }
            }
        }
        if (!props) {
            props = {};
        }
        if (!tag) {
            tag = 'div';
        }
        if (!children) {
            children = [];
        }
        const el = document.createElement(tag);
        for (const k in props) {
            if (k == 'style') {
                Object.assign(el.style, props[k]);
            }
            else if (k == 'attr') {
                for (const ak in props[k]) {
                    el.setAttribute(ak, props[k][ak]);
                }
            } else {
                el[k] = props[k];
            }
        }
        children.forEach(c => el.appendChild(Array.isArray(c) ? this.createDOM.apply(this, c) : c));
        if (parent) {
            parent.appendChild(el);
        }
        return el;
    }

    /**
     * BIOHARZARD!
     */
    setPlatform(platform: PlatformContract) {
        this.platform = platform;
    }

    private clearInLineStyle() {
        const DOMElement = this.epubContainer.getElementById('epub').children;
        for (let index = 0; index < DOMElement.length; index++) {
            if (DOMElement[index]['style']['cssText']) {
                DOMElement[index]['style']['cssText'] = '';
            }
        }
    }

    private caseProps(prop: string) {

        switch (prop.toLowerCase()) {
            case 'font-family':
            case 'font-size':

                if (this._style[prop]) {
                    this.styleAsterisk.push(`${prop}: ${this._style[prop]} !important`);
                }

                break;

            case 'background-color':
            case 'color':
            case 'filter':
                this.styleBody.push(`${prop}: ${this._style[prop]} !important`);
                break;

            default:
                throw new Error('No property found');
        }
    }

    private forceReplacementStyle() {

        delete this._style['theme'];

        for (const prop in this._style) this.caseProps(prop);


        if (!this.styleAsterisk.length && !this.styleBody.length) return;

        const styleReplaceElement: Element = this.epubContainer.getElementsByTagName('replace-style').item(0);
        styleReplaceElement.innerHTML = '';

        if (!styleReplaceElement) return;

        const stringStyle = `* { ${this.styleAsterisk.join('; ')} } body { ${this.styleBody.join('; ')} }`;

        const styleElement = document.createElement('style');
        styleElement.type = 'text/css';
        styleElement.appendChild(document.createTextNode(stringStyle));

        styleReplaceElement.appendChild(styleElement);

        this.styleBody = [];
        this.styleAsterisk = [];
    }

    private isVoiceOverRunning(documentHTML: any, cssUri: string, handle: any) {

        const div = (display, stretchWidth) => {
            return this.createDOM("div", documentHTML.body, [
                ['link', { attr: { href: cssUri, rel: 'stylesheet' } }],
                ['replace-style', {}],
                ["img", {
                    src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=',
                    title: 'VOLTAR',
                    style: {
                        width: '100px',
                        height: '100%',
                        float: 'left',
                        display: display
                    },
                    attr: {
                        onclick: "document.body.dispatchEvent( new Event('swiperight') )"
                    }
                }],
                [{
                    id: 'stretch',
                    draggable: false,
                    style: {
                        display: 'inline-bock',
                        overflow: 'hidden',
                        float: 'left',
                        width: stretchWidth,
                        '-webkit-user-select': 'none',
                        '-moz-user-select': 'none',
                        '-ms-user-select': 'none',
                        'user-select': 'none',
                        'user-drag': 'none',
                        '-webkit-user-drag': 'none'
                    }
                }, [
                    [{
                        id: 'epub',
                        style: {
                            position: 'relative',
                            webkitColumns: 'auto',
                            // webkitColumnGap: '60px',
                            webkitColumnWidth: 'inherit',
                            columnFill: 'balance',
                            overflow: 'visible'
                        }
                    }, Array.from(documentHTML.body.childNodes)]
                ]],
                ["img", {
                    src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=',
                    title: 'PRÓXIMO',
                    style: {
                        width: '100px',
                        height: '100%',
                        float: 'left',
                        display: display
                    },
                    attr: {
                        onclick: "document.body.dispatchEvent( new Event('swipeleft') )"
                    }
                }]
            ]);
        };

        let display = 'none';
        let stretchWidth = '100%';
        let isVoiceOverExists = false;

        if (typeof MobileAccessibility !== 'undefined') {
            MobileAccessibility.isVoiceOverRunning(isRunning => {
                if (isRunning) {
                    display = 'visible';
                    stretchWidth = 'calc(100% - 200px)';
                    MobileAccessibility.speak('PÁGINA CARREGADA. TOQUE NAS LATERAIS PARA PASSAR DE PÁGINA.');
                }

                return handle(div(display, stretchWidth));
            });
        }

        return handle(div(display, stretchWidth));
    }

    private swipeLeft(resolve) {
        this.updateEpubContainer();

        this.HammerJS.on('swipeleft', (event) => {
            ++this.countSwipes;

            this.leftPosition += -(window.outerWidth);

            if (this.countSwipes === this.totalColumns) {
                this.epubContainer.getElementById('epub').style.display = 'none';
                this.leftPosition = 0;
                this.countSwipes = 0;
                this.epubContainer.getElementById('epub').style.left = this.leftPosition + 'px';
                resolve(++this.pageNumber);
                this.HammerJS.destroy();
            } else {
                this.epubContainer.getElementById('epub').style.display = 'visible';
                this.epubContainer.getElementById('epub').style.left = this.leftPosition + 'px';
            }
        });
    }

    private swipeRight(resolve) {
        this.updateEpubContainer();
        this.HammerJS.on('swiperight', (event) => {
            --this.countSwipes;

            this.leftPosition += window.outerWidth;

            if (this.countSwipes < 0) {
                this.epubContainer.getElementById('epub').style.display = 'none';
                this.countSwipes = this.totalColumns - 1;
                this.leftPosition = -((window.outerWidth) * (this.totalColumns - 1));
                this.epubContainer.getElementById('epub').style.left = this.leftPosition + 'px';
                resolve(--this.pageNumber);
                this.HammerJS.destroy();
            } else {
                this.epubContainer.getElementById('epub').style.display = 'visible';
                this.epubContainer.getElementById('epub').style.left = this.leftPosition + 'px';
            }
        });
    }

    private doubletap(resolve) {
        this.epubContainer.body.addEventListener('doubletap', (event) => {
            resolve(false);
        })
    }

    private setTotalColumns(EPub) {
        // tslint:disable-next-line:max-line-length
        const columns = EPub.scrollWidth / (parseInt(EPub.style.webkitColumnWidth));
        this.totalColumns = Math.ceil(columns);
        this.fitImages(EPub);
    }

    private fitImages(EPub): void {
        const imgs = EPub.getElementsByTagName('img');

        for (let img of imgs) {
            if (img) {
                img.style.width = 'auto';
                img.style.maxWidth = '50vh';
            }
        }
    }

    private getMaxWidth(element) {
        return Math.max(element.clientWidth, element.offsetWidth, element.scrollWidth);
    }

    private getMinWidth(element) {
        const stretch = this.epubContainer.getElementById('stretch');
        return window.outerWidth;
    }

    private getMaxHeight(element) {
        const e = element.cloneNode(true);
        e.style.visibility = 'hidden';
        element.appendChild(e);
        const height = Math.max(e.clientHeight, e.offsetHeight, e.scrollHeight);
        element.removeChild(e);
        e.style.visibility = 'visible';
        return height;
    }

    private getMinHeight(element, height?: number) {
        height = (height || window.screen.height) - 100;
        const e = element.cloneNode(true);
        e.style.visibility = 'hidden';
        element.appendChild(e);
        element.removeChild(e);
        e.style.visibility = 'visible';
        return height;
    }

    private getHeight(element) {
        const e = element.cloneNode(true);
        e.style.visibility = 'hidden';
        element.appendChild(e);
        const height = e.scrollHeight + 0;
        element.removeChild(e);
        e.style.visibility = 'visible';
        return height;
    }

    private getWidth(element) {
        const e = element.cloneNode(true);
        e.style.visibility = 'hidden';
        element.appendChild(e);
        const width = e.scrollWidth + 0;
        element.removeChild(e);
        e.style.visibility = 'visible';
        return width;
    }

    private linkContrast(reset?: string) {
        const allLinks = this.epubContainer.getElementById('epub').getElementsByTagName('a');
        if (!reset) {
            for (let i = 0, l = allLinks.length; i < l; i++) {
                this.epubContainer.getElementById('epub').getElementsByTagName('a')[i].style.color = '#43a047';
                this.epubContainer.getElementById('epub').getElementsByTagName('a')[i].style.textDecoration = 'underline';
            }
        } else {
            for (let i = 0, l = allLinks.length; i < l; i++) {
                this.epubContainer.getElementById('epub').getElementsByTagName('a')[i].style.color = '#0000FF';
                this.epubContainer.getElementById('epub').getElementsByTagName('a')[i].style.textDecoration = 'underline';
            }
        }
    }

    private replicateToChildren(children, reset = false) {
        const allContainers = children;
        if (allContainers.length) {
            if (!reset) {
                for (let i = 0; i < allContainers.length; i++) {
                    for (const _sty in this._style) {
                        children[i].style[_sty] = this._style[_sty] + ' !important';
                        if (children[i].hasChildNodes()) {
                            this.replicateToChildren(children[i]);
                        }
                    }
                }
            } else {
                for (let i = 0; i < allContainers.length; i++) {
                    if (allContainers[i].style['cssText']) {
                        this.epubContainer.getElementById('epub').children[i].style['cssText'] = '';
                    }
                }
            }
        }
    }

    private getSizeOf(args, item?: any) {
        if (this.platform.is(PlatformEnum.DESKTOP)) {
            return this.desktopSizeOf(args);
        }

        const mobileSize = async (item) => {
            const dimensions = await this.mobileSizeOf(args);
            item.style['max-width'] = dimensions['width'] + 'px';
            return dimensions;
        };

        return mobileSize(item);
    }

    private desktopSizeOf(args) {
        this.sizeOf = window.require('image-size');
        return this.sizeOf(args.replace('file://', ''));
    }

    private async mobileSizeOf(args) {
        return new Promise((resolve, reject) => {

            window.resolveLocalFileSystemURL(args, (fileEntry: any) => {

                fileEntry.file(file => {
                    const reader = new FileReader();

                    reader.onloadend = function (evt) {

                        let image = new Image();
                        image.onload = function (evt) {

                            const width = this['width'];
                            const height = this['height'];
                            image = null;
                            return resolve({ width, height });
                        };

                        image.src = evt.target['result'];
                    };

                    reader.readAsDataURL(file);

                }, reject);

            }, error => {
                //
            });

        });
    }
}

import { Injectable, Inject } from '@angular/core';
import { WebWorkerService } from 'angular2-web-worker';
import { PackageOpfService } from './package-opf.service';
import { ReaderContract } from './contracts/reader.contract';
import { BookReaderCollectionContract } from './contracts/book-reader-collection.contract';
import { EPubItemContract } from './contracts/e-pub-item.contract';
import { EPubSpineContract } from './contracts/e-pub-spine.contract';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';

@Injectable()
export class EpubReaderService implements ReaderContract {
    readonly booksPATH = '';
    private _worker: WebWorkerService = new WebWorkerService();
    private readonly metaPath: string = 'META-INF/';
    private readonly container: string = 'container.xml';
    private domParser: DOMParser = new DOMParser();
    private bookId: string;
    private XHTML: EPubItemContract[] = [];
    private IMAGE: EPubItemContract[] = [];
    private CSS: EPubItemContract[] = [];
    private FONT: EPubItemContract[] = [];
    private PLS: EPubItemContract[] = [];

    init(bookId: string, handler: any) {
        this.bookId = bookId;
        this.loadPackageInfo(handler);
    }

    getCollection(): BookReaderCollectionContract {
        const collection: BookReaderCollectionContract = <BookReaderCollectionContract>{};
        collection.pages = this.XHTML;
        collection.fonts = this.FONT;
        collection.images = this.IMAGE;
        collection.styles = this.CSS;
        collection.pls = this.PLS;
        return collection;
    }

    getBookId(): string {
        return this.bookId;
    }

    destroy() {
        this.packageOpf.baseURL = '';
        for (let prop in this) {
            if (prop != 'metaPath'
                && prop != 'container'
                && prop != 'domParser' && prop != 'booksPATH'
                && prop != '_worker' && prop != 'packageOpf' && prop != 'fileSystem') {
                delete this[prop];
            }
        }
    }

    getFile(objectSpineContract: EPubSpineContract, fileHandler: any, errorHandler?: any) {
        const path = this.getBasePath(this.packageOpf.getPage(objectSpineContract).href, false);

        this.fileSystem.request(path)
            .readFile()
            .get()
            .then(fileHandler)
            .catch(errorHandler);
    }

    findPage(pageName: string, bookList: Array<EPubSpineContract>) {

        const epubItem: EPubItemContract[] = this.packageOpf.manifest;
        const page = pageName;

        const match = page.match(/([^#]+)(#.*)?/);
        const anchor = match[2];
        const summary = epubItem.filter(item => item.properties && item.properties.indexOf('nav') !== -1);
        const currentPageFile = epubItem.filter(item => item.href.indexOf(match[1]) !== -1);

        const spineItem = this.packageOpf.spine.filter((spine, index) => {
            return spine.itemref.idref === currentPageFile[0].id;
        });

        let pageNumber;
        bookList.filter((itemBookList: EPubSpineContract, i) => {
            if (itemBookList.itemref.idref === spineItem[0].itemref.idref) {
                pageNumber = i;
                return true;
            }
            return false;
        });

        return { spineItem: spineItem[0], summary: summary[0], anchor, pageNumber };
    }

    getBasePath(path?: string, baseURL: boolean = true) {
        return this.booksPATH + this.bookId + '/' + ((baseURL) ? this.packageOpf.baseURL : '') + path;
    }

    private hydrate(mediaType: string, handler: any) {
        this._worker
            .run(
                (args: Array<any>) => {
                    const manifest = args[0];
                    const mType = args[1];
                    const result = [];

                    manifest.forEach(epubItem => {
                        if (epubItem && epubItem.hasOwnProperty('mediaType') && epubItem.mediaType.indexOf(mType) !== -1) {
                            result.push(epubItem);
                        }
                    });

                    return result;
                },
                [this.packageOpf.manifest, mediaType]
            )
            .then(handler);
    }

    private loadPackageInfo(handler: any) {
        this.getPackagePath((packagePATH: any) => {
            const packPATH = this.getBasePath(packagePATH);
            this.fileSystem.request(packPATH)
                .readFile()
                .toString(data => {
                    this.packageOpf.loadXMLString(data, (isReady: boolean) => {
                        this.hydrateAll(handler);
                    });
                }, error => {
                    console.log(error, 'ERROR PATH', packPATH);
                });
        }, error => {
            console.log(error, 'getPackagePath ERROR');
        });
    }

    private getPackagePath(successHandler: any, errorHandler?: any) {
        const containerPATH = this.getBasePath(this.metaPath + this.container, false);

        this.fileSystem
            .request(containerPATH)
            .readFile()
            .toString(response => {

                const containerObject = this.domParser.parseFromString(
                    response,
                    'text/xml'
                );

                const rootFiles = containerObject.getElementsByTagName('rootfile');

                const file = rootFiles.item(0).attributes.getNamedItem('full-path').textContent;
                let fileName, filePath;

                if (file.indexOf('/') !== -1) {
                    const arrayFile = file.split('/');
                    filePath = arrayFile[0];
                    fileName = arrayFile[1];
                    this.packageOpf.baseURL = filePath + '/';
                }

                successHandler((fileName) ? fileName : file);

            }, errorHandler);

    }

    private hydrateAll(handler: any) {
        this.hydrate('xhtml', (resultXHTML: any) => {

            this.XHTML = resultXHTML;

            this.hydrate('css', (resultCSS: any) => {

                this.CSS = resultCSS;

                this.hydrate('image', (resultIMAGE: any) => {
                    this.IMAGE = resultIMAGE;
                });

                this.hydrate('font', (resultFONT: any) => {
                    this.FONT = resultFONT;
                });

                this.hydrate('pls', (resultPLS: any) => {
                    this.PLS = resultPLS;
                });

                handler(this.packageOpf.spine);

            });

        });
    }

    getInstalledManifest() {
        return this;
    }

    constructor(private packageOpf: PackageOpfService, @Inject('FileSystemService') private fileSystem: LedFileSystemContract) { }
}

import { Injectable, Inject } from '@angular/core';
import { BookEvent } from "../../events/book/book.event";
import { HotspotContract } from './contracts/hotspot.contract';
import { OED } from '../content-manager/installer.service';
import { HotspotTypeEnum } from './hotspot-type.enum';
import { DownloadManagerService } from '../content-manager/download-manager.service';
import { BookDao } from '../../daos/book.dao';
import { UserNotesService } from './user-notes.service';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { VaultToolsService } from '../vault/vault-tools.service';


@Injectable()
export class HotspotLegacyService {

    private currentUser: string;
    private currentPageId: number;
    private currentBookId: string;
    private currentBookManifest: any;
    private pageManifestURLsById: any;
    private currentPageBookHotpots: Array<HotspotContract>;
    private hotspotDecriptionKey: string;
    private wordsHotspotsByPageId: any;

    constructor(@Inject('FileSystemService') private fileSystem: LedFileSystemContract,
        @Inject('PlatformService') private platform: PlatformContract,
        private bookEvent: BookEvent,
        private book: BookDao,
        private userNotes: UserNotesService,
        private tools: VaultToolsService) {
    }

    public init(bookBaseManifest: any, currentUser: string): Promise<any> {
        this.currentPageId = 0;
        this.currentUser = currentUser;
        this.currentBookManifest = bookBaseManifest;

        return new Promise<any>(async (resolve, reject) => {
            this.currentBookId = bookBaseManifest.book_id;
            const BOOK_KEY = await this.book.getKeyById(this.currentBookId);
            this.hotspotDecriptionKey = BOOK_KEY + this.tools.salt;

            Promise.all([
                this.loadBookHotspots(),
                this.getWordsHotspot(),
                this.userNotes.init(bookBaseManifest, currentUser)
            ]).then((response) => {
                console.log(response);
                this.bookEvent.EchoAnnotationsUpdated.emit(true);
                resolve(true);
            }).catch((error) => {
                reject('erro inicializando HotspotLegacyService: ' + JSON.stringify(error));
            });
        });
    }

    public setCurrentPageId(pageId: number): Promise<any> {
        this.currentPageId = pageId;

        return new Promise<any>((resolve, reject) => {
            this.fileSystem.request(this.pageManifestURLsById[this.currentPageId]).readFile().toJSON(pageManifestObj => {
                this.currentPageBookHotpots = [];
                for (let oed of pageManifestObj.oeds) {
                    this.currentPageBookHotpots.push(this.getHotspotFromOED(oed));
                }
                resolve();
            })
        });
    }

    public getAllUserHotspots(): Array<HotspotContract> {
        return this.userNotes.getAllUserHotspots();
    }

    public getUserHotspots(): Array<HotspotContract> {
        return this.userNotes.getPageHotspots(this.currentPageId);
    }

    public getOEDHotposts(): Array<HotspotContract> {
        return this.currentPageBookHotpots;
    }

    public getWordHotspot(word: string): any {
            if (this.wordsHotspotsByPageId) {
                const indexes = Object.keys(this.wordsHotspotsByPageId);
                let pageObjects = [];
                indexes.map((valor, index) => {
                    const regWord = new RegExp(word, 'g');
                    if (valor.match(regWord)) {
                        const keyWord = indexes[index];
                        pageObjects = pageObjects.concat(this.wordsHotspotsByPageId[keyWord]);
                    }
                });
                return pageObjects;
            } else {
                return [];
            }
    }

    saveUserHotspot(hotspot: HotspotContract): Promise<any> {
        hotspot.pageId = this.currentPageId;
        return this.userNotes.saveHotspot(hotspot);
    }

    public deleteUserHotspot(hotspotId: number): Promise<any> {
        return this.userNotes.deleteHotspot(hotspotId, this.currentPageId);
    }

    decryptHotspot(hotspot: any): HotspotContract {
        let decryptionIndex = hotspot.id % this.hotspotDecriptionKey.length;
        let decryptionOffset = (this.hotspotDecriptionKey.charCodeAt(decryptionIndex)) / 100.0;
        hotspot.x -= decryptionOffset;
        hotspot.y -= decryptionOffset;
        hotspot.width -= decryptionOffset;
        hotspot.height -= decryptionOffset;
        return hotspot;
    }

    public getWordsHotspot(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.fileSystem.request(this.currentBookId + '/word_hotspot_map.json').readFile().toJSON(words => {
                this.wordsHotspotsByPageId = words;
                resolve();
            },
                (error) => {

                    console.log('getWordsHotspot - erro: ' + error);
                    this.wordsHotspotsByPageId = {};
                    reject(error);
                });
        })
    }

    private loadBookHotspots(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.pageManifestURLsById = {};
            try {
                if (this.currentBookManifest.pages_manifest_urls != null) {
                    for (let i = 0; i < this.currentBookManifest.pages_manifest_urls.length; i++) {
                        let pageId = this.currentBookManifest.pages_manifest_urls[i].page_id;
                        this.pageManifestURLsById[pageId] = this.currentBookManifest.pages_manifest_urls[i].url;
                    }
                }
                resolve()
            }
            catch (err) {
                reject('Error loading book hotspots (' + JSON.stringify(err) + ')');
            }
        });
    }

    private getHotspotFromOED(oed: OED): HotspotContract {
        const oedUrl = (oed.url) ? oed.url.replace(/\\/g, "/") : '';

        let hotspot: HotspotContract = {
            id: oed.id,
            book_id: this.currentBookId,
            type: HotspotTypeEnum[oed.type],

            x: oed.x,
            y: oed.y,
            width: oed.w,
            height: oed.h,

            description: oed.description,

            resource: {
                parameters: oed.parameters,
                autoplay: oed.autoplay,
                url: oedUrl
            }
        }

        if (!hotspot.resource.url.startsWith('/')) {
            hotspot.resource.url = '/' + hotspot.resource.url;
        }

        return this.decryptHotspot(hotspot);
    }
}

import { EPubSpineContract } from './contracts/e-pub-spine.contract';
import { PackageContract } from './contracts/package.contract';
import { Injectable } from '@angular/core';
import { EPubItemContract } from './contracts/e-pub-item.contract';
import { EPubDocumentCreatorContract } from './contracts/e-pub-document-creator.contract';
import { EPubMetadataContract } from './contracts/e-pub-metadata.contract';
import { setTimeout } from 'timers';
@Injectable()
export class PackageOpfService implements PackageContract {
  baseURL: string = '';
  metadata: EPubMetadataContract  = <EPubMetadataContract>{};
  manifest: EPubItemContract[]    = [];
  spine: EPubSpineContract[]      = [];
  private DOMParser: DOMParser = new DOMParser();
  private XMLObject: Document;
  private loader: {metadata: boolean, manifest: boolean, spine: boolean} = {metadata: false, manifest: false, spine: false};

  constructor() {}

  loadXMLString(xmlString: string, handler?: any): void {
    this.XMLObject = this.DOMParser.parseFromString(xmlString, 'text/xml');
    this.loadMetaData();
    this.loadManifest();
    this.loadSpine();

    handler(this.isReady());

  }

  getPage(objectSpineContract: EPubSpineContract): EPubItemContract {
    const page = this.manifest.filter(manifestItem => {
      return manifestItem.id === objectSpineContract.itemref.idref;
    });

    return page[0];
  }

  private isReady(): boolean {
    return (this.loader.metadata && this.loader.manifest && this.loader.spine);
  }

  private loadMetaData() {
    const metadata = this.XMLObject.getElementsByTagName('metadata');
    const currentMeta = metadata.item(0);
    const childrenMeta = currentMeta.children;

    this.metadata['dc'] = <EPubDocumentCreatorContract>{};
    this.metadata['dc']['contributor'] = [];
    this.metadata['meta'] = [];

    for (let i = 0; i < childrenMeta.length; i++) {
      this.setMetadataAttribute(
        childrenMeta.item(i).tagName,
        childrenMeta.item(i).textContent,
        childrenMeta.item(i).attributes
      );
    }

    this.loader.metadata = true;
  }

  private setMetadataAttribute(
    tagName: string,
    value: string,
    attributes: NamedNodeMap
  ) {
    if (tagName.indexOf('dc') !== -1) {
      const tag = tagName.split(':');
      const dcAttr = tag[1];

      if (dcAttr.indexOf('contributor') !== -1) {
        this.metadata['dc']['contributor'].push(value);
      }

      if (attributes.getNamedItem('id')) {
        if (attributes.getNamedItem('id')) {
          this.metadata['dc'][dcAttr] = {
            id: attributes.getNamedItem('id').value,
            value: value
          };
        } else {
          this.metadata['dc'][dcAttr] = { value: value };
        }
      }
    }

    if (tagName.indexOf('meta') !== -1) {
      const property = attributes.getNamedItem('property').value;
      this.metadata.meta.push({ property: property, value: value });
    }
  }

  private loadManifest() {
    const manifest = this.XMLObject.getElementsByTagName('manifest');
    const currentManifest = manifest.item(0);
    const childrenManifest: HTMLCollection = currentManifest.children;
    const object: EPubItemContract[] = [];

    for (let i = 0; i < childrenManifest.length; i++) {
      object[i] = <EPubItemContract>{id: childrenManifest.item(i).getAttribute('id'), 
                                        href: childrenManifest.item(i).getAttribute('href'),
                                        mediaType: childrenManifest.item(i).getAttribute('media-type'),
                                        properties: childrenManifest.item(i).getAttribute('properties')};
    }

    this.manifest = object;

    this.loader.manifest = true;
  }

  private loadSpine() {
    const spine = this.XMLObject.getElementsByTagName('spine');
    const currentSpine = spine.item(0);
    const childrenSpine = currentSpine.children;
    const object: EPubSpineContract[] = [];
    
    for (let i = 0; i < childrenSpine.length; i++) {
      object[i] = <EPubSpineContract> { 
        itemref: {
          idref: childrenSpine.item(i).getAttribute('idref'), linear: childrenSpine.item(i).getAttribute('linear')
        }
      };
    }

    this.spine = object;
    this.loader.spine = true;

  }

  private camelCase(input: string) {
    return input.toLowerCase().replace(/-(.)/g, function(match, group1) {
      return group1.toUpperCase();
    });
  }
}

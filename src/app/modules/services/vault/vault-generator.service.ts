import { Injectable } from "@angular/core";

@Injectable()
export class VaultGeneratorService {
    UUID() {
        let date = new Date().getTime();

        const uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
            let random16Bits = ((date + Math.random() * 16) % 16) | 0;
            date = Math.floor(date / 16);
            return (c == "x" ? random16Bits : (random16Bits & 0x3) | 0x8).toString(16);
        });

        return uuid;
    }
}

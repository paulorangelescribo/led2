import { Injectable, Inject } from '@angular/core';
import { BookEvent } from "../../events/book/book.event";
import { PlatformEnum } from '../contracts/platform/platform.enum';
import { LayoutFileSystemEnum } from '../contracts/file-system/layout-file-system.enum';
import { LedFileSystemContract } from '../contracts/file-system/led-file-system.contract';
import { PlatformContract } from '../contracts/platform/platform.contract';
import { AppEvent } from '../../events/app.event';

@Injectable()
export class VaultToolsService {
    noImageCover: string;
    /**
     *
     * @type {{string}} Base64 string image
     */
    private _image: string;

    /**
     * @type {{string}} BOOK_SALT
     */
    private _salt: string = "_DEV_DACCORD_PLURI";

    constructor(@Inject('FileSystemService') private fs: LedFileSystemContract,
        @Inject('PlatformService') private platform: PlatformContract,
        private appEvent: AppEvent,
        private bookEvent: BookEvent) { }

    get salt(): string {
        return this._salt;
    }

    initModals() {
        $('.modal').modal({
            dismissible: false,
            opacity: 0.5,
            inDuration: 300,
            outDuration: 200,
            startingTop: '4%',
            endingTop: '10%',
            ready: (modal, trigger) => {
                this.bookEvent.EchoAriaHiddenModal.emit(true);
                $('.modal-title-link').focus();
            },
            complete: (modal, trigger) => {
                this.bookEvent.EchoAriaHiddenModal.emit(false);
            }
        });
    }

    initSideNav() {
        $('.button-collapse').sideNav({
            edge: 'left',
            closeOnClick: true,
            draggable: false,
            onOpen: () => {
                $('body').css('width', '100%');
                this.appEvent.EchoAriaHidden.emit(false);
            },
            onClose: () => {
                this.appEvent.EchoAriaHidden.emit(true);
                // this.router.navigateByUrl('/login  ');
            }
        });
    }

    sanitizeWord(name: string) {
        name = (name || "").toLowerCase().replace(/\s+/, " ");
        name = name.replace(/[áàãâä]/g, "a");
        name = name.replace(/[óòõôö]/g, "o");
        name = name.replace(/[úùûü]/g, "u");
        name = name.replace(/[éèêë]/g, "e");
        name = name.replace(/[íìîï]/g, "i");
        name = name.replace(/[ý]/g, "y");
        name = name.replace(/[ç]/g, "c");
        name = name.replace(/[ñ]/g, "n");
        name = name.replace(/[ß]/g, "ss");
        return name;
    }

    getImageBase64(input: string, directory: string = '') {
        return new Promise(async (resolve, reject) => {
            try {
                const currentPath = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/'));
                let requestPath = `${currentPath.replace('/desktop.html', '')}/assets/base64/${input}.txt`;

                if (!this.platform.is(PlatformEnum.DESKTOP)) {
                    let dirEntry;

                    if (this.platform.is(PlatformEnum.ANDROID)) {
                        requestPath = `cdvfile://localhost/assets/www/assets/base64/${input}.txt`;
                    } else {
                        requestPath = `cdvfile://localhost/bundle/www/assets/base64/${input}.txt`;
                    }

                }

                this.fs.request(requestPath, LayoutFileSystemEnum.NO_ROOT_PATH)
                    .readFile()
                    .toString(resolve, reject);
            } catch (error) {
                console.log(error);
                reject(error);
            }
        });
    }

    async loadNoImageCoverInMemory() {
        const imageBase64 = <string> await this.getImageBase64('empty-book-image');
        this.noImageCover = imageBase64;
    }
}

import { Component, OnInit, Input, Inject } from '@angular/core';
import { LegacyBookContract } from '../../../../contracts/legacy-book.contract';
import { LogProvider } from '../../../../providers/log.provider';
import { BookService } from '../../../services/book.service';
import { BookDao } from '../../../daos/book.dao';
import { ModalContract } from '../../../../contracts/modal.contract';
import { DialogContract } from '../../../services/contracts/dialog/dialog.contract';

@Component({
    selector: 'app-book-info',
    templateUrl: './book-info.component.html',
    styleUrls: ['./book-info.component.css']
})
export class BookInfoComponent implements ModalContract, OnInit {
    id: string = 'BookInfoComponent';
    title: string;
    @Input() book: LegacyBookContract = <LegacyBookContract>{};
    @Input() bookStatus: string;

    constructor(@Inject('DialogService') private dialog: DialogContract,
        private bookService: BookService,
        private bookDao: BookDao) {
    }

    ngOnInit() {
        //
    }

    show() {
        $('#' + this.id).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

    getBookCover(book: LegacyBookContract) {
        if (!book.hasOwnProperty('download_urls')) {
            return;
        }
        return this.bookService.getBookCoverURL(book);
    }

    setData(book: LegacyBookContract) {
        this.book = book;
    }

    setBookStatus(bookStatus: string) {
        this.bookStatus = bookStatus;
    }

    uninstallBook() {
        const bookLedId = this.book.book_led_id;
        this.dialog.onConfirm('Você realmente deseja desinstalar esta obra?',
            'Desinstalação/Remoção de obra').then(async (result: boolean) => {
                if (result) {
                    await this.bookDao.uninstall(this.book);
                }
            });
    }

}

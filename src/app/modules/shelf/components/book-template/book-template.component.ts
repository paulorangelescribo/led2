import { Component, ElementRef, Input, OnInit, ViewChild, Inject } from '@angular/core';
import { LegacyBookContract } from "../../../../contracts/legacy-book.contract";
import { BookDao } from "../../../daos/book.dao";
import { Router } from "@angular/router";
import { BookService } from "../../../services/book.service";
import { BookEvent } from "../../../events/book/book.event";
import { NetworkContract } from '../../../services/contracts/network/network.contract';
import { DialogContract } from '../../../services/contracts/dialog/dialog.contract';
import { BookStatusEnum } from '../shelf/book-status.enum.component';
import { ImageCover } from '../shelf/image-cover.enum.component';

@Component({
    selector: "app-book-shelf-template",
    templateUrl: "./book-template.component.html",
    styleUrls: ["./book-template.component.css"]
})
export class BookTemplateComponent implements OnInit {
    @Input() book: any;
    @Input() bookStatus: any;
    @Input() progress: any;
    @Input() bookUpgreadable: boolean;
    @Input() bookExpired: boolean;
    @ViewChild("cover") cover: ElementRef;
    @ViewChild("coverLink") coverLink: ElementRef;
    @ViewChild("loadCover") loadCover: ElementRef;

    constructor(private bookDao: BookDao,
        @Inject('NetworkService') private network: NetworkContract,
        @Inject('DialogService') private dialog: DialogContract,
        private router: Router,
        private bookService: BookService,
        private bookEvent: BookEvent) { }

    ngOnInit() {
        this.chooseCoverClass();
    }

    chooseCoverClass(status?: string) {
        let _status = status || this._getStatus();
        let newStatus;
        switch (_status) {
            case BookStatusEnum.ATUALIZANDO:
            case BookStatusEnum.INICIALIZANDO:
                newStatus = ImageCover.LOADING;
                // this.loadCover.nativeElement.className = ImageCover.LOADING;
                this.coverLink.nativeElement.classList.add(ImageCover.DISABLED_IMG);
                break;
            case BookStatusEnum.BAIXADO:
            case BookStatusEnum.BAIXANDO:
            case BookStatusEnum.INSTALANDO:
            case BookStatusEnum.UNDEFINED_STATUS:
            case BookStatusEnum.NOVO:
                newStatus = ImageCover.OPEN_BOOK;
                this.cover.nativeElement.classList.add(newStatus);
                break;
            case BookStatusEnum.ERROR:
            case BookStatusEnum.EXPIRED:
                newStatus = ImageCover.DISABLED_IMG;
                this.cover.nativeElement.classList.add(newStatus);
                break;
        }
        if (!this.bookExpired) {
            newStatus = ImageCover.DISABLED_IMG;
            this.cover.nativeElement.classList.add(newStatus);
        }
        if (newStatus === ImageCover.LOADING) {
            // chupis
        }
    }

    isDownloadable() {
        const status = this._getStatus();
        return (!this.bookStatus || status === BookStatusEnum.NOVO) && this.bookExpired === true;
    }

    chooseWhatToDo(book: LegacyBookContract) {
        const status = this._getStatus();
        if (status == BookStatusEnum.OPEN_BOOK) {
            return this._checkForUpdates(book.book_led_id);
        } else if (status == BookStatusEnum.DOWNLOAD_BOOK || !status) {
            this.bookStatus = BookStatusEnum.INICIALIZANDO;
            return this._checkConnectionAndBookDownload(book);
        } else {
            return;
        }
    }

    openBookInfo() {
        this.bookEvent.EchoShowBookInfo.emit(this.book);
    }

    showLoader() {
        return this.bookStatus === BookStatusEnum.INICIALIZANDO || this.bookStatus === BookStatusEnum.ATUALIZANDO;
    }

    errorHandler(event, bookId) {
        event.target.src =
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAACRCAIAAAC634qpAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAADkpJREFUeNrsnUtMG8cbwNfYwJoQ40dIMRQCJiayQYmcNTSNSZu2Rn0EqqohqiLkXir52CtHlyPXHDlVpYdKVKqqOH1pG/WBCQ9tVIMxxgjnYSsOtmEhhXhZY/t/mP5H213vYlIbgzPfBWzvzM785vu+mZ3HfrJsNoshyU8qEAIEC8EqtSjAn0wmwzDM3t4eIsKTmpoahULxL81Kp9PJZBKhEQrDMHwzzGQySK1ySiqVQj4LOXgEC8FCsBAsJAgWgoVgIVgIFoKFBIqinCpD0/T4+DhN0xqNxmAwUBT1+eefazQapFk5xO/3J5NJm81G0zRFUQRBFJAUhmEysGCxu7u7ubmpVCqRrfEknU5D4gczQ4qiKIqKRqPgo91uJwgCx3EMw6LR6NjYGPher9c7nU5oGrdu3eJmYrfbbTYb/CiWkCsej4ckSfB/f38/QRCgMG63G8Mwrq1NTEz4/X7wv8PhMBgMMBOGYSiKgvloNBqCILglKaQZut1ut9sNSWEYtri4CEhhGBYKhTAMg+BomhbLhyRJUMmDJoTgxH5iGIabG/gf/jQ2NgZJgVbklaRgsEKhEEVRGIYZDIbh4WGXy3Xjxg273c4FB34FBYXNy1Uol8sFVABklWdCoRfnJucVEswCNzY28u5CkiRoBoIgXC7X8PCwXq8H13CZFgwWrDOoldlshkpO0zTQuMbGRoBDrATcJAdKCAwH1NDj8XCnxoVKCuwUKhpsEhzHQQPjON7b28urWsFgQesDxRV2Q5BFZ2cnt5F5SgELDdDkmRBIMpkEVRVTLqiksEkgCJCnUqmEfgO6ObHbFWvoABHo9XpYUF59SJK8desWKBls1XwScv0OBDE5OSlsMGiDIEOJrIo7KIUNEo1GecoFTYlhmJGREa5RCPsaoP/g+wMlhGKz2YD28bwbVCKSJKEjZxgGFBjHcYZhkskkwzCgLrAngVUrmGbBZidJEjQgtAUxl8wzKODgh4eHIYg8EwpLAgrD8zVAScW0Htg4wzCAI8MwUDe5w4vCaBZBEH6/PxQKhUKh0dFReBuCIGChh4eHQSvBYRFFUWazed9OI2fCfZUrJ1+bzQb76NHRUaCAdrvdbreHQiHQwFzbJAgif1gH8FkOhwN2hUDMZjPscYCq89pKoqN54YQ8F867HgwauLkBY8dx3Ol0csc6Go2mv7+/v78fPe4U5XEHTdGg+SwEC8FCsBAsJAgWgoVgIVgIVpnKoS6y0jQdiUTC4TBN0wzDsCx78uTJnp4eo9Eok8kQrH/k4cOHXq/30aNHW1tbmUwGfh+LxcLhcHt7O0EQbW1tFRVHWtOLPuuwtbX1+++/z8/Pp9NpKXdQUXH27Fmr1drW1gZPNBy1WYfiwlpbW/vuu+/W1ta4X8rlchzH9/b2dnd3+U0nk3V0dFgsFqPReES07JBgra+vf/PNN4lEAn5z6tSprq6u5uZmlUqVSqW8Xu/9+/e5RxggTYPB0NPTYzAYSo7sMGClUqlvv/02GAxCK+vp6ent7T1x4gT3ssePH09PTy8vL3MdGUzS0dHR3d3d2tpaQmSHAcvr9X7//fcgc5lMduXKlbfeeivnlZlMJhAI3Lt3LxKJCH+trKw0Go0XL15sb28vOaxiudL5+Xl4oLi+vv7SpUsSrt1sNre3t3u93unpad5eh1Qq5ff7g8FgR0fHa6+91tzcXMJBhvyLL74A/BiGqaysLFS+JEmyLAv+TyaTT58+VSqVWq1WrKoKhaKpqclkMikUCpqmYVqoffF4fGFhYX19XalUqtXqQwOUzWahwRUL1tTUFPTc2Wx2Y2MjEAgkEgm1Wn3y5EmxVNXV1QaDoa2tLZVKxWIxYbljsdjy8nIsFlOpVCqVqkxgzc7O8kYGmUwmFov5fL5kMqnT6STWgWtra00mU1NT0/b29ubmJu/Xvb29WCy2sLCwtbWlVqt5PcaxhDU9PS0cRoEbhcPhYDCYSqV0Ol1VVZVYDlqt1mw2a7Xara2t7e1tYbcQjUYDgcDOzo5ara6pqSlDWEAYhnnw4MHKyopcLq+vrxcbGcjl8oaGBrPZXFNTE4vFeI4MwzCWZcPh8NLSEsuy9fX1EuiPuhnuu5Xn+fPnwWAwHA7X1NRoNBox319ZWdnS0nLu3LlsNkvTtPB4MsuyDx8+XFlZUSgUp06dksvlxwzWzMxMnvueNjc3l5aWYrFYXV2dhM+uqakxGo0tLS3Pnz9fX1/PiX55eTkej6tUqrq6uvKEBQoUj8f9fv/29rZGo5FwQHV1dSaTqaGhYXNz8++//xZekEgkZDLZuXPnigGrWIPSF3hA2d3dnZmZCQQCFovFYrGIaZlcLjeZTAaDYX5+fnZ2lvvsCa2yHCb/8pzS+e233xYXFy9dunT+/Hmx6Zrq6uru7m6j0Tg3N3f//n2uFgufzAumAUdzmi0ej9++ffvrr78OBoMS72FSq9V9fX0mk4k3EDtmmlWQJ7hHjx6Fw+HOzs7XX38958ZfaJi8oVxZwaqurm5tba2rq1Or1YlEYmNjIxKJ5NSITCazsLAQCoUIgujp6ck5Xufd6/jBksB0+fJli8XCUweGYXw+371793L2oTs7O3/88UcgEBgYGHj11VelYQmnxo66z8qpWSqVamhoyGq1CseNOI5brdbPPvuspaVFLM9YLPbgwYN9e97i+azDg1VVVTU4OKjT6SRSsSwrl8slhqY526AMzbC7u1ua1OPHj3/66aetrS2Ja3Ka2KFNBx7S0EGhUIDzNBImxiXV3Nycc4avPGHxKtDS0lJdXS128bNnz3744QdIqra2tq+v79q1a3nCOrTljEOCJezCoGxvb9+5cycej4OPSqXyvffe0+l0jY2Nra2tL6Nmic1nplIpkiThuk5FRcXVq1fb2trAR4vFcmgD4KPls4RfptPpX3/9dWVlBdb5jTfe6Orqghe0trbyEpanZvH8SM6h5tTU1MLCAvxIEITVauUhPn36NG/CpPzNULhUMzs7OzMzAz+eP3/+zTffFFY7nyWccoP15MkT7seFhYU///wTqonRaHz77bdzdmq8RaCXwgzj8Tjs71ZXV+/evQur3djY2NfXJzZJK7HqUbawMAybn5/HMCwSifz8889wMlOj0bz//vsSa3+8dcPiPSSX8nFH+Kjs9Xrb2tru3r27s7MDxxMffPCBVqsVy4RhmKdPn2JHRg4PVjqdvnPnDuwWq6ur3333Xe5ZSqEEAgFe91dazSqWGeYcWEFScrn8nXfekd5FxLIst7ss56GD9L7Qy5cvgxPeEvLLL788e/bspYAlvSy8urq6sbEh9ms6nf7xxx+XlpaEP+WEdWgP0sXyWRL7isCw68svvzSZTBcuXOC6LZZlV1dXPR6P2Ot7Shtxo1iwurq6gsEgbyzKUx+fz+fz+aqqquA+xEQiIT3PWZ4OXqfT3bx588KFC/teybLs2v9l3xnh0mpWEa29trb2o48+unbtWgG36JWng4ditVqHhoYk3huCYP1LTp8+PTg4ODAw8N9fV1ieDl7Y+BcvXmxtbZ2amvrrr79eeLWqbH2WULRabX9//yeffNLU1IQ0Ky8xGo1nzpyZmZmZnZ0V7qxFmsWXqqqqK1euvIDjz2azQl48oy7einQp92c1NDRcv379xo0bDQ0NeSbJOSitr6/nLkoW75RPiXf+gYM7Z86cmZuboyjqoFYJ6Vy/ft3j8bAs29nZmecC2vGDBeTEiRNXr141mUyTk5M+n+8FHneAH0ylUkU9cHGEtkm+8sorH3/88eDgoMQmPwkHX1VVVeyjKUdrA65MJuvs7ARvJ5+bm8s5n5XNZkt1iu4obsBVKpW9vb1DQ0MWi4U3V1Wesw4FeUj68MMPb968Cbc+vIyD0gPJ2bNnW1pawPsyGYYhCKKE56XR2yT3EfQ2ybLzWQgWgoVgIUGwECwEC8E63nLsA6u53W6KonAcHxgYKNTqZHnCAqQIguCFQEDPhqV/NhTVrPHxcW4gCV4YMhjUDAYv83g8k5OTYG8fiBdDEMTi4iI3GJdGo7HZbNzjYR6Ph6IouMGIG6ktn2BqQLMwDHO5XGNjYyAwjdPphIGfQA7gprz4bgaDweFwwGt4Udj+k4MHYcjGx8dz/gpuCXdBgmAeQrugaRpWD8OwiYkJGMILCEmSX331lfA4hkQwNShwKyFsY7jhad9dhgXzWS6Xi2GYiYkJEEcG+AjeNTAw0/DwMGh/3rYGh8Oh0WhA2/r9foIgKIoCcXf0ev2nn36K4zjgGI1GhXFkYHBHiXJCvQCMYPQVEGGMq7wHCtN3YM2C8cgwkSAvwNmBEEBgik6o0hqNBhAE5RZGaoO34IYb2jeYGhS9Xs8NzQYj8RSwi8zXDOGKS84Swzb3eDyjo6PcMIJQ/H4/wASygvlAHcRxHFDj3mLfYGpcAVxAyCsxGyRJcmRkZGRkJB/TLsqg1Gw2OxwOCNTj8fC82/j4+MTEBI9sPiIdTE3CbcHwfRILa8UaZ0mH7AMuw+l0hkIht9tN03TOeF96vb63txfUHLp/EJwXcIGdKS+hWDC1nJZI0/STJ09AgYUNU3SfxTDM7du3eX6U11uBahgMhpw71hwOh8vlcjqd0IMII7VB4xV2XmLB1MQskRs28VBH8NxgejabLeftuWMlaGvSQ2oQkc3v90ejURipDWhHTjsVBlMTs0TojHAcF5YWBvQD4yyuo4D/i0WfPkDIPl4Q1pwOHnad+QQsAxlyi2W3251OZ07KwmBqYpYIkx/IOaLHnQI/7qApGjSfhWAhWAgWgoUEwUKwECwEC8FCsJAgWAgWglVy+WemtKKiQiaTRSKRfN5X9fJIbW0td83hn8m/TCbDsizLssU72XgcpbKyEsdx+F4dWWkPeCCfVbbyvwEA1nd0boLTADUAAAAASUVORK5CYII=";
    }

    getBookUrl() {
        return this.bookService.getBookCoverURL(this.book);
    }

    private async _checkForUpdates(bookLedId: string) {
        if (this.bookUpgreadable) {
            const isUpdated = await this.bookDao.installBookUpdate(bookLedId);
            this.bookStatus = BookStatusEnum.ATUALIZANDO;
            if (isUpdated) {
                this.bookUpgreadable = false;
                this.bookStatus = BookStatusEnum.BAIXADO;
            }
        } else {
            this.router.navigateByUrl("/reader/" + bookLedId);
        }
    }

    private _getStatus() {
        let status;
        switch (this.bookStatus) {
            case BookStatusEnum.NOVO:
            case BookStatusEnum.BAIXANDO:
            case BookStatusEnum.BAIXADO:
                status = BookStatusEnum.OPEN_BOOK;
                break;
            case BookStatusEnum.INICIALIZANDO:
            case BookStatusEnum.ATUALIZANDO:
                status = BookStatusEnum.DOWNLOAD_BOOK;
                break;
            case BookStatusEnum.ERROR:
            case BookStatusEnum.EXPIRED:
                status = BookStatusEnum.ERROR;
                break;
            default:
                status = BookStatusEnum.UNDEFINED_STATUS;
        }
        return status;
    }

    private _checkConnectionAndBookDownload(book) {
        if (!this.network.isConnected() || this.network.isLimitedConnection()) {
            this.dialog
                .onConfirm("Parece que você não está conectado à internet ou está em uma internet limitada, deseja prosseguir o download?", "Confirmar download", ["Continuar", "Cancelar"])
                .then(result => {
                    if (result) {
                        this.bookStatus = BookStatusEnum.BAIXANDO;
                        this.loadCover.nativeElement.style.display = 'none';
                        this.bookDao.downloadBook(book);
                    }
                });
        } else {
            this.bookStatus = BookStatusEnum.BAIXANDO;
            this.bookDao.downloadBook(book);
        }
    }
}


import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DropdownComponent} from '../../../global-components/components/dropdown/dropdown.component';
import {AppEvent} from '../../../events/app.event';

@Component({
  selector: 'app-shelf-dropdown',
  templateUrl: './shelf-dropdown.component.html',
  styleUrls: ['./shelf-dropdown.component.css']
})
export class ShelfDropdownComponent extends DropdownComponent implements OnInit {
  @Input() id: string = 'shelfdp';
  @Input() idContent: string = 'shelfdp-2';

  constructor(private app: AppEvent) {
    super();
  }

   Action(action: string) {
    if (action === 'managementSpace') {
      this.app.EchoShowManagementSpace.emit(true);
    }
  }

  ngOnInit() {
    $(document).ready(() => {
      this.options.alignment = 'right';
      $('#' + this.id).dropdown(this.options);
    });
  }
}

import { Component, OnInit, EventEmitter, DoCheck, OnDestroy, Input } from "@angular/core";
import { BookEvent } from "../../../events/book/book.event";
import { ContentManagerService } from "../../../services/content-manager/content-manager.service";
import { BookService } from "../../../services/book.service";
import { AuthGuardService } from "../../../services/guard/auth-guard.service";
import { ModalContract } from "../../../../contracts/modal.contract";

@Component({
    selector: "app-shelf-component-filter-modal",
    templateUrl: "./shelf-filter-modal.component.html",
    styleUrls: ["./shelf-filter-modal.component.css"]
})
export class ShelfFilterModalComponent implements ModalContract, OnInit {
    id: string = 'ModalFilter';
    title: string;
    filterList: Array<string> = [];
    filter: Array<any> = [];
    filterOptionSelected: Array<any> = [];
    isSubmitted = false;

    constructor(private bookEvent: BookEvent,
        private auth: AuthGuardService) { }
    ngOnInit() {
        if (this.auth.has("filter")) {
            this.filter = this.filterOptionSelected = this.auth.get("filter");
            this._checkMarkedOption();
        }

        this.bookEvent.EchoDiscipline.subscribe(value => {
            this.filterList = value.filter(item => {
                return item != '';
            });
        });
    }
    private _checkMarkedOption() {
        if (this.filterList && this.filter) {
            $(document).ready(() => {
                for (let i = 0, count = this.filter.length; i < count; i++) {
                    const checkedFilter: any = document.getElementById(this.filter[i]);
                    if (!checkedFilter.checked) {
                        document.getElementById(this.filter[i])["checked"] = true;
                    }
                }
            });
        }
    }

    chooseFilters(filterData, event) {
        if (event.target.checked) {
            this.filterOptionSelected.push(filterData);
        } else {
            this.filterOptionSelected = this.filterOptionSelected.filter(item => item.indexOf(filterData) === -1);
        }
    }

    submit() {
        this.filter = this.filterOptionSelected;
        this.auth.currentSession = { filter: this.filter };
        this.bookEvent.EchoBookFilterField.emit([...this.filter]);
        this.isSubmitted = true;
    }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

    resetCheckboxies() {
        const inputs = document.getElementsByTagName("input");
        let checkBoxies: Array<HTMLInputElement> = [];

        for (let i = 0; i < inputs.length; i++) {
            if (
                inputs
                    .item(i)
                    .getAttribute("type")
                    .indexOf("checkbox") !== -1
            )
                checkBoxies.push(inputs.item(i));
        }

        this.filter.forEach(item => {
            checkBoxies.forEach(checkbox => {
                if (item.toLowerCase() == checkbox.id.toLowerCase()) checkbox["checked"] = true;
            });
        });
    }
}

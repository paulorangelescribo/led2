import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfManagementSpaceComponent } from './shelf-management-space.component';

describe('ShelfManagementSpaceComponent', () => {
  let component: ShelfManagementSpaceComponent;
  let fixture: ComponentFixture<ShelfManagementSpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfManagementSpaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfManagementSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

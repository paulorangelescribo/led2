import { Component, Input, OnInit } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { UserSettingsService } from '../../../services/user-settings.service';
import { AppEvent } from '../../../events/app.event';
import { ModalContract } from '../../../../contracts/modal.contract';

@Component({
  selector: 'app-shelf-management-space',
  templateUrl: './shelf-management-space.component.html',
  styleUrls: ['./shelf-management-space.component.css']
})

export class ShelfManagementSpaceComponent implements OnInit, ModalContract {
    id: string;
    title: string = 'Limite de uso de espaço';

  @Input() public isLimitlessChecked: boolean = false;
  @Input() public isLimitChecked: boolean = false;

  @Input() public minSpace: string;
  @Input() public space: string;
  @Input() public lastSpace: string;

  inputSpace: HTMLInputElement;
  buttonChangeLimitWrap: HTMLElement;
  inputSpaceWrap: HTMLElement;

  constructor(protected bookEvent: BookEvent,
              protected userSettings: UserSettingsService,
              protected appEvent: AppEvent) {}

  setLimitValues(): void {
    this.minSpace = '200';
    this.space = this.userSettings.getUserDefinedSpaceLimit().toString();
  }

  checkLayoutSettings(): void {

    if (this.isLimitChecked) {

      this.inputSpaceWrap.style.display = 'block';
      if (parseInt(this.space) === Number.MAX_SAFE_INTEGER) this.space = this.minSpace;
      if (this.space !== '' && parseInt(this.space) >= parseInt(this.minSpace)) {
        this.buttonChangeLimitWrap.style.display = 'block';
      } else {
        this.buttonChangeLimitWrap.style.display = 'none';
      }

    } else if (this.isLimitlessChecked) {

      this.buttonChangeLimitWrap.style.display = 'block';
      this.inputSpaceWrap.style.display = 'none';
      this.space = String(Number.MAX_SAFE_INTEGER);
    }
  }

  addSettingsEvents(): void {

    this.inputSpace = <HTMLInputElement>document.getElementById('inputSpace');
    this.buttonChangeLimitWrap = (document.getElementById('buttonChangeLimitWrap') as HTMLElement);
    this.inputSpaceWrap = (document.getElementById('inputSpaceWrap') as HTMLElement);

    this.inputSpace.addEventListener('input', () => {
      this.space = this.inputSpace.value;
      this.checkLayoutSettings();
    });

    $('input[type=\'radio\']').each((index, element) => {
      $(element).change(() => {
        if ($('#yesLimit').is(':checked')) {
          this.isLimitChecked = true;
          this.isLimitlessChecked = false;
        } else {
          this.isLimitChecked = false;
          this.isLimitlessChecked = true;
        }
        this.checkLayoutSettings();
      });
    });
  }

  resetLimitSettings(): void {

    if (this.lastSpace === undefined || parseInt(this.lastSpace) === Number.MAX_SAFE_INTEGER) {

      this.isLimitChecked = false;
      this.isLimitlessChecked = true;

      this.inputSpaceWrap.style.display = 'none';
      this.buttonChangeLimitWrap.style.display = 'block';

      this.space = Number.MAX_SAFE_INTEGER.toString();
    }
    else {

      this.isLimitChecked = true;
      this.isLimitlessChecked = false;

      this.inputSpaceWrap.style.display = 'block';
      this.buttonChangeLimitWrap.style.display = 'block';

      this.space = this.lastSpace.toString();
    }
  }

  changeLimit(): void {
    this.lastSpace = this.space.toString();

    if (this.isLimitChecked) {
      const limitInBytes = parseInt(this.space) * 1024 * 1024;
      this.userSettings.setUserDefinedSpaceLimit(limitInBytes);
    } else if (this.isLimitlessChecked) {
      this.userSettings.setUserDefinedSpaceLimit(Number.MAX_SAFE_INTEGER);
    }

    this.hide();
  }

  ngOnInit() {
    this.addSettingsEvents();

    this.appEvent.EchoSettingsReady.subscribe(() => {
      this.setLimitValues();

      let bytes = this.userSettings.getUserDefinedSpaceLimit();
      if (bytes === Number.MAX_SAFE_INTEGER) {
        this.isLimitChecked = false;
        this.isLimitlessChecked = true;
      }
      else {
        this.isLimitChecked = true;
        this.isLimitlessChecked = false;
        this.space = Math.round((bytes / (1024 * 1024))).toString();
      }

      this.checkLayoutSettings();
    });

    this.appEvent.EchoResetSpaceSettingsModal.subscribe((isReset) => {
      if (isReset) this.resetLimitSettings();
    });
  }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }

}

import { Component, OnInit, Input } from '@angular/core';
import { BookEvent } from '../../../events/book/book.event';
import { AuthGuardService } from '../../../services/guard/auth-guard.service';
import { ModalContract } from '../../../../contracts/modal.contract';

@Component({
  selector: 'app-shelf-component-reorder-modal',
  templateUrl: './shelf-reorder-modal.component.html',
  styleUrls: ['./shelf-reorder-modal.component.css']
})
export class ShelfReorderModalComponent implements ModalContract, OnInit {
    id: string = 'ModalOrder';
    title: string;
    orderType: string;
    orderList: Array<{display: string, value: string}> = [
                                                            { display: 'Padrão', value: 'SORT_DEFAULT'},
                                                            { display: 'Título', value: 'SORT_TITLE'},
                                                            { display: 'Autor', value: 'SORT_AUTHOR'},
                                                            { display: 'Disciplina', value: 'SORT_DISCIPLINE'}
                                                        ];

    constructor(private auth: AuthGuardService,
                private bookEvent: BookEvent) {}

    ngOnInit() {
        //
    }

    chooseOrder(typeOrder: string) {
        this.orderType = typeOrder;
        this.auth.currentSession = {sort: typeOrder};
        this.bookEvent.EchoBookSort.emit(this.orderType);
    }

    show() {
        $(`#${this.id}`).modal('open');
    }

    hide() {
        $(`#${this.id}`).modal('close');
    }
}

import { Component, OnInit } from '@angular/core';
import {SearchbarComponent} from '../../../global-components/components/searchbar/searchbar.component';
import {AppEvent} from '../../../events/app.event';
import { AuthGuardService } from '../../../services/guard/auth-guard.service';

@Component({
  selector: 'app-shelf-searchbar-component',
  templateUrl: './shelf-searchbar.component.html',
  styleUrls: ['./shelf-searchbar.component.css']
})
export class ShelfSearchbarComponent extends SearchbarComponent {

  private searchValue: string;

  constructor(private app: AppEvent, private auth: AuthGuardService) {
    super();
  }

  search(event: any) {
    this.searchValue = event.target.value.toLowerCase();
    this.auth.currentSession = { search: this.searchValue };
    this.app.EchoSearch.emit({search: this.searchValue});
  }

}

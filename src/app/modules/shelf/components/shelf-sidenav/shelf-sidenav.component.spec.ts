import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShelfSidenavComponent } from './shelf-sidenav.component';


describe('ShelfSidenavComponent', () => {
  let component: ShelfSidenavComponent;
  let fixture: ComponentFixture<ShelfSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfSidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

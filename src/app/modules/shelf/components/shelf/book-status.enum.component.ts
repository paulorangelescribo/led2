export enum BookStatusEnum {
    UNDEFINED_STATUS = "",
    INICIALIZANDO = "inicializando",
    NOVO = "novo",
    BAIXANDO = "baixando",
    BAIXADO = "baixado",
    INSTALANDO = "instalando",
    ATUALIZANDO = "atualizando",
    ERROR = "error",
    EXPIRED = "expired",
    OPEN_BOOK = "openBook",
    DOWNLOAD_BOOK = "downloadBook",
    LOADING = 'loading'
}

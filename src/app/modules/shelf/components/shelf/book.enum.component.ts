export enum BookEnumComponent {
    INVALID,
    AVAILABLE,
    INSTALLED,
    UNAVAILABLE,
    SUSPENDED,
    EXPIRED
}

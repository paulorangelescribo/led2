export enum ImageCover {
    LOADING = "loading",
    OPEN_BOOK = "open-book",
    DISABLED_IMG = "disabled-img"
}

import { Component, OnInit, Input, ViewChild, Inject, OnDestroy, DoCheck } from "@angular/core";
import { SelectivePreloadingStrategy } from "../../../../selective-preloading-strategy";
import { VaultToolsService } from "../../../services/vault/vault-tools.service";
import { UserSettingsService } from "../../../services/user-settings.service";
import { AuthGuardService } from "../../../services/guard/auth-guard.service";
import { AppEvent } from "../../../events/app.event";
import { BookDao } from "../../../daos/book.dao";
import { ContentManagerService } from "../../../services/content-manager/content-manager.service";
import { BookEvent } from "../../../events/book/book.event";
import { BookInfoComponent } from "../book-info/book-info.component";
import { LegacyBookContract } from "../../../../contracts/legacy-book.contract";
import { NetworkContract } from "../../../services/contracts/network/network.contract";
import { DialogContract } from "../../../services/contracts/dialog/dialog.contract";
import { BookService } from "../../../services/book.service";
import { PlatformContract } from "../../../services/contracts/platform/platform.contract";
import { WebWorkerService } from "angular2-web-worker";
import { BookEnumComponent } from "./book.enum.component";
import * as IScroll from "iscroll";
import { PlatformEnum } from "../../../services/contracts/platform/platform.enum";

@Component({
    selector: "app-shelf-component",
    templateUrl: "./shelf.component.html",
    styleUrls: ["./shelf.component.css"]
})
export class ShelfComponent implements OnInit, DoCheck, OnDestroy {
    sortChanged: boolean = false;
    isSearching: boolean = false;
    isBookListLoaded: boolean = false;
    @Input() progress: Array<any> = [{}];
    @Input() bookStatus: Array<any> = [{}];
    @Input() bookList: Array<LegacyBookContract> = [];
    @Input() emptyBookImage: string = "";
    @Input() bookListUpdated: string;
    @Input() bookUpgradeable: Array<any> = [];
    @Input() updatedBookList: Array<any> = [];
    @Input() wrapperDivId: string = '';
    @Input() scrollableDivId: string = '';
    @ViewChild(BookInfoComponent) BookInfoComponent: BookInfoComponent;

    private searchCriteria: string;
    private sortCriteria: string;
    private filterCriteria: Array<any> = [];
    private ORDER_TYPES = [{ type: "SORT_DEFAULT" }, { type: "SORT_TITLE" }, { type: "SORT_AUTHOR" }, { type: "SORT_DISCIPLINE" }];
    private observables = [];

    constructor(
        @Inject("NetworkService") private network: NetworkContract,
        @Inject("DialogService") private dialog: DialogContract,
        @Inject("PlatformService") private platform: PlatformContract,
        private book: BookDao,
        private contentManager: ContentManagerService,
        private bookEvent: BookEvent,
        private appEvent: AppEvent,
        private userSettings: UserSettingsService,
        private authGuard: AuthGuardService,
        private bookService: BookService,
        private tools: VaultToolsService,
        private worker: WebWorkerService
    ) {
        this.observables.push(
            this.appEvent.EchoProgress.subscribe((response: any) => {
                this.bookStatus[response.book_id] = response.book_status;
                this.checkDownloads(response);
            })
        );

        this.observables.push(
            this.bookEvent.EchoShowBookInfo.subscribe((response: LegacyBookContract) => {
                this.openBookInfo(response.book_led_id);
            })
        );
        this.platform.ready().then(() => this.iSCrollInit);
    }

    private iSCrollInit() {
        if (this.platform.is(PlatformEnum.IOS)) {
            this.wrapperDivId       = "wrapper";
            this.scrollableDivId    = "scroller";

            $("#wrapper").ready(() => {
                const iScroll = new IScroll("#wrapper", {
                    mouseWheel: true,
                    click: true,
                    bounceTime: 1200
                });
                console.log(iScroll);
            });
        }
    }

    ngDoCheck() {
        if (this.authGuard.has("search")) {
            if (!this.isSearching) {
                this.isSearching = true;
                this.searchBook({ search: this.authGuard.get("search") });
            }
        }

        if (this.authGuard.has("sort")) {
            if (this.sortChanged && this.bookList.length > 0) {
                this.sortChanged = false;
                this.sortCriteria = this.authGuard.get("sort");
                if (this.sortCriteria && this.sortCriteria.trim() !== "") {
                    const orderExists = this.ORDER_TYPES.filter(order => {
                        return order.type.toUpperCase().indexOf(this.sortCriteria) !== -1;
                    });

                    if (orderExists.length > 0) {
                        return this.sort();
                    }
                }
            }
        }
    }

    ngOnInit() {
        this.tools.loadNoImageCoverInMemory();
        this.tools.initSideNav();
        this.tools.initModals();

        this.userSettings.init(this.authGuard.user().led_user_id, this.appEvent);

        this.appEvent.EchoVisibleComponent.emit({
            toolbar: true,
            navbar: true
        });

        $("body").css("background-color", "white");

        this.updateBookList();
        this.loadBookList();
        this.observables.push(
            this.bookEvent.EchoBookDeleted.subscribe(bookLedId => {
                if (this.bookStatus.hasOwnProperty(bookLedId)) {
                    delete this.bookStatus[bookLedId];
                }
            })
        );

        this.observables.push(
            this.appEvent.EchoSearch.subscribe(response => {
                this.searchCriteria = response.search;
                this.searchBook(response);
            })
        );

        this.observables.push(
            this.bookEvent.EchoBookSort.subscribe(response => {
                this.sortChanged = true;
                this.sortCriteria = response;
            })
        );

        this.observables.push(
            this.bookEvent.EchoBookFilterField.subscribe(response => {
                this.filterCriteria = response;
                this.filterBookList();
            })
        );

        if (this.authGuard.has("filter")) {
            this.filterCriteria = this.authGuard.get("filter");
            this.filterBookList();
        }
    }

    ngOnDestroy() {
        if (this.observables.length) {
            for (let i = 0, count = this.observables.length; i < count; i++) {
                this.observables[i].unsubscribe();
            }
        }
        this.sortChanged    = false;
        this.isSearching    = false;
    }

    listBooks() {
        return this.bookList;
    }

    openBook(book) {
        this.checkConnectionAndBookDownload(book);
    }

    getBookCoverURL(book: LegacyBookContract): string {
        return this.bookService.getBookCoverURL(book);
    }

    openBookInfo(bookLedId: string) {
        const book = this.bookList.filter(item => {
            return item.book_led_id.indexOf(bookLedId) !== -1;
        });
        this.BookInfoComponent.id = "BookInfoComponent";
        this.BookInfoComponent.title = "Informações da Obra";
        this.BookInfoComponent.setData(book[0]);
        this.BookInfoComponent.setBookStatus(this.bookStatus[bookLedId]);
        this.BookInfoComponent.show();
    }

    isBookAuthorized(bookStatus: string): boolean {
        let isAuthorized = true;
        switch (parseInt(bookStatus)) {
            case BookEnumComponent.INVALID:
            case BookEnumComponent.EXPIRED:
            case BookEnumComponent.UNAVAILABLE:
            case BookEnumComponent.SUSPENDED:
                isAuthorized = false;
                break;
        }
        return isAuthorized;
    }

    async bookUpdate() {
        return await this.book.updateBookList();
    }

    async errorHandler(event, book) {
        event.target.src = this.emptyBookImage;
    }

    private checkConnectionAndBookDownload(book) {
        if (!this.network.isConnected() || this.network.isLimitedConnection()) {
            this.dialog
                .onConfirm("Parece que você não está conectado à internet ou está em uma internet limitada, deseja prosseguir o download?", "Confirmar download", ["Continuar", "Cancelar"])
                .then(result => {
                    if (result) {
                        this.downloadBook(book);
                    }
                });
        } else {
            this.downloadBook(book);
        }
    }

    private async downloadBook(book: any) {
        try {
            const manifest = await this.book.install(book);
            this.contentManager.onReceiveBookManifest(book.book_led_id, manifest, book.download_urls);
        } catch (error) {
            this.dialog.onAlert(error.message || error, "[Error] Download book");
        }
    }

    private async loadBookList() {
        try {
            this.bookList = <LegacyBookContract[]>await this.book.fetchData();
            this.sort();
            this.loadCoreShelf();
        } catch (error) {
            Materialize.toast(error.message, 1500);
        }
    }

    private loadCoreShelf() {
        if (this.bookList == null) return;
        this.loadBookStatus();
        this.changeBookStatus();
        let filterList: Array<string> = [];
        const filterItens: Array<string> = [];

        this.bookList.forEach(d => {
            filterItens.push(d.book_discipline);
        });

        filterList = filterItens.filter(function(el, pos, self) {
            return self.indexOf(el) === pos;
        });

        this.bookEvent.EchoDiscipline.emit(filterList);

        this.contentManager.loadBookStatusData();
    }

    private loadBookStatus() {
        this.contentManager
            .getBookStatusData()
            .then((booksStatus: Array<any>) => {
                this.worker
                    .run(
                        (args: any) => {
                            const progress = [];
                            const bookList = args[0];

                            for (const book of bookList) {
                                progress[book.book_led_id] = "novo";
                            }

                            return progress;
                        },
                        [this.bookList]
                    )
                    .then(async progress => {
                        this.progress = progress;
                        // tslint:disable-next-line:forin
                        for (const statusBookLedId in booksStatus) {
                            this.bookStatus[statusBookLedId] = booksStatus[statusBookLedId].book_status;
                            try {
                                const a = await this.book.bookHasUpdate(statusBookLedId, this.updatedBookList || []);
                                this.bookUpgradeable[statusBookLedId] = a;
                            } catch (err) {
                                console.log(err);
                            }
                        }
                    });
            })
            .catch(error => {
                console.log("loadBookStatus -> contentManager.getBookStatusData()" + JSON.stringify(error));
            });
    }

    private searchBook(response: any) {
        this.bookList = this.bookList.filter(item => {
            const searchValue = this.tools.sanitizeWord(item.book_title);
            return searchValue.indexOf(this.tools.sanitizeWord(response.search)) !== -1;
        });
        if (response.search == "") this.loadBookList();
    }

    private async filterBookList() {
        if (!this.filterCriteria.length) return this.loadBookList();

        this.bookList = await this.book.fetchData();
        this.sort();

        this.bookList = this.bookList.filter(book => {
            for (let i = 0; i < this.filterCriteria.length; i++) {
                const fieldSearch: string = "book_discipline";
                const fieldSearchValue: string = this.filterCriteria[i].toLowerCase();

                if (this.filterCriteria[i] === undefined || book[fieldSearch].toLowerCase().indexOf(fieldSearchValue) !== -1) {
                    return true;
                }
            }

            return false;
        });
    }

    private sort() {
        switch (this.sortCriteria) {
            case "SORT_DEFAULT":
                return this.sortDefault();

            case "SORT_TITLE":
                return this.sortTitle();

            case "SORT_AUTHOR":
                return this.sortAuthor();

            case "SORT_DISCIPLINE":
                return this.sortDiscipline();

            default:
                return this.sortDefault();
        }
    }

    private sortDefault() {
        return this.bookList.sort(function(a, b) {
            if (a.book_publisher_id > b.book_publisher_id) {
                return 1;
            }
            if (a.book_publisher_id < b.book_publisher_id) {
                return -1;
            }
            return 0;
        });
    }

    private sortTitle() {
        return this.bookList.sort(function(a, b) {
            if (a.book_title < b.book_title) {
                return -1;
            }

            if (a.book_title > b.book_title) {
                return 1;
            }

            return 0;
        });
    }

    private sortAuthor() {
        return this.bookList.sort(function(a, b) {
            if (a.book_author > b.book_author) {
                return 1;
            }
            if (a.book_author < b.book_author) {
                return -1;
            }
            return 0;
        });
    }

    private sortDiscipline() {
        return this.bookList.sort((a, b) => {
            return a.book_discipline > b.book_discipline ? 1 : a.book_discipline < b.book_discipline ? -1 : 0;
        });
    }

    private checkDownloads(bookProgress) {
        const selector = "#" + bookProgress.book_id;
        const progressBarIdSelector = $(selector);
        if (!progressBarIdSelector.is(":visible")) {
            progressBarIdSelector.show();
        }

        const loader = progressBarIdSelector.find(".determinate");

        if (bookProgress.book_status === "baixado") {
            loader.hide();
        }

        if (bookProgress.book_progress < 100) {
            this.progress[bookProgress.book_id] = bookProgress.book_progress * 100.0;
        }
    }

    private changeBookStatus() {
        this.bookList.map(book => {
            if (this.bookStatus[book.book_led_id]) {
                switch (book.book_status) {
                    case BookEnumComponent.INVALID:
                    case BookEnumComponent.EXPIRED:
                    case BookEnumComponent.UNAVAILABLE:
                    case BookEnumComponent.SUSPENDED:
                        this.bookStatus[book.book_led_id] = BookEnumComponent[book.book_status];
                        break;
                }
            }
        });
    }

    private updateBookList() {
        this.book.updateBookList().then(books => {
            if (books._body) {
                books = JSON.parse(books._body).books;
                this.updatedBookList = books;
            }
        });
    }
}

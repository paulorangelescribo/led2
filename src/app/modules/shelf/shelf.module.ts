import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShelfRouterModule } from './shelf.router.module';
import { GlobalComponentsModule } from '../global-components/global-components.module';
import { FormsModule } from '@angular/forms';
import { ShelfComponent } from './components/shelf/shelf.component';
import { ShelfDropdownComponent } from './components/shelf-dropdown/shelf-dropdown.component';
import { ShelfNavbarComponent } from './components/shelf-navbar/shelf-navbar.component';
import { ShelfManagementSpaceComponent } from './components/shelf-management-space/shelf-management-space.component';
import { ShelfToolbarComponent } from './components/shelf-toolbar/shelf-toolbar.component';
import { ShelfFilterModalComponent } from './components/shelf-filter-modal/shelf-filter-modal.component';
import { ShelfSearchbarComponent } from './components/shelf-searchbar/shelf-searchbar.component';
import { ShelfReorderModalComponent } from './components/shelf-reorder-modal/shelf-reorder-modal.component';
import { ShelfSidenavComponent } from './components/shelf-sidenav/shelf-sidenav.component';
import { BookInfoComponent } from './components/book-info/book-info.component';
import { ShelfMenuComponent } from './components/shelf-menu/shelf-menu.component';
import { ShelfUserInfoComponent } from './components/shelf-user-info/shelf-user-info.component';
import { BookTemplateComponent } from './components/book-template/book-template.component';
import { VaultToolsService } from '../services/vault/vault-tools.service';


@NgModule({
    imports: [CommonModule, FormsModule, GlobalComponentsModule, ShelfRouterModule],
    declarations: [
        ShelfComponent,
        BookInfoComponent,
        BookTemplateComponent,
        ShelfDropdownComponent,
        ShelfNavbarComponent,
        ShelfManagementSpaceComponent,
        ShelfToolbarComponent,
        ShelfFilterModalComponent,
        ShelfSearchbarComponent,
        ShelfReorderModalComponent,
        ShelfSidenavComponent,
        ShelfMenuComponent,
        ShelfUserInfoComponent
    ]
})
export class ShelfModule {}

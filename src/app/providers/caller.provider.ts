import { Injectable } from '@angular/core';
import { JsonObject } from './json-object.provider';
import { BaseUri, BaseUriEnum } from './enums/base-uri.enum';
declare var startApp;
declare var cordova;

@Injectable()
export class CallerProvider {
  private PlatformIntentInstance: any;
  private JSONObject: JsonObject;
  private intent: string;

  constructor() {}

  /**
   * Call External Application
   *
   * @param {string} intent
   * @param {JsonObject} jsonObject
   * @returns
   * @memberof CallerProvider
   */
  call(intent: string, jsonObject: JsonObject) {
    this.JSONObject = jsonObject;

    return new Promise((resolve, reject) => {
      try {
        this.intent = intent;

        this.callIntent();

        resolve();

      } catch (exception) {
        reject(exception);
      }
    });
  }

  private callIntent() {
    this.definePlatform()
        .check();
  }

  private definePlatform() {

    if (cordova.platformId.toLowerCase() === 'android') {
      const options = {
        uri:
          BaseUri.getEnum(BaseUriEnum.ALFACON_NOTES) +
          this.intent +
          this.JSONObject.getUriParams(),
        intentstart: 'startActivity',
        flags: ['FLAG_ACTIVITY_NEW_TASK'],
        package: 'br.com.alfaconcursos.anotacoes'
      };

      this.PlatformIntentInstance = startApp.set(options);
    }

    if (cordova.platformId.toLowerCase() === 'ios') {
      this.PlatformIntentInstance = startApp.set(
        BaseUri.getEnum(BaseUriEnum.ALFACON_NOTES)
      );
    }

    return this;

  }

  private check() {
    this.PlatformIntentInstance.check(() => {
        this.initalizeIntent();
    }, () => {
        this.getAppInPlatformStore();
    });
  }

  /**
   * Init new intent
   * @param intent
   */
  private initalizeIntent() {

    if (cordova.platformId.toLocaleLowerCase() === 'android') {
      this.PlatformIntentInstance.start();
    }

    if (cordova.platformId.toLocaleLowerCase() === 'ios') {
      window.location.href =
      BaseUri.getEnum(BaseUriEnum.ALFACON_NOTES) +
      this.intent +
      this.JSONObject.getUriParams();
    }
  }

  private getAppInPlatformStore() {

    if (cordova.platformId.toLowerCase() === 'ios') {
      startApp
        .set(BaseUri.getEnum(BaseUriEnum.APPLE_STORE) + 'alfaconnotes')
        .start();
    }

    if (cordova.platformId.toLowerCase() === 'android') {
      startApp
        .set({
          uri:
          BaseUri.getEnum(BaseUriEnum.ANDROID_STORE) +
            'details?id=br.com.alfaconcursos.anotacoes',
          intentstart: 'startActivity'
        })
        .start();
    }
  }
}

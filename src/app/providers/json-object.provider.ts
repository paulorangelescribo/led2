export class JsonObject {
    private DataObject: any = {};

    constructor(json?: Object, key?: string) {

        if (key) {
            this.DataObject[key] = json;
        } else {
            this.DataObject = json;
        }
    }

    getJson(key?: string) {
        return (key) ? this.DataObject[key] : this.DataObject;
    }

    toString(key?: string) {
        const jsonValue = (key) ? this.DataObject[key] : this.DataObject;
        return JSON.stringify(jsonValue);
    }

    getUriParams(): string  {

        const data = Object.keys(this.DataObject);

        let params;

        if (data.length === 1) {
            params = '?' + data[0] + '=' + this.toString(data[0]);
        } else {
            params = this.toString();
        }

        return params;
    }

    getJSON(key?: string, stringify: boolean = false) {

        let json = this.DataObject;

        if (key) {
            json = json[key];
        }
        if (stringify) {
            json = JSON.stringify(json);
        }

        return json;
    }

    exists() {
        return (this.DataObject);
    }
}

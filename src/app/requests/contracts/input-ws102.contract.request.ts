export interface InputWs102ContractRequest {
  book: any;
  deviceId: string;
  login: string;
  password: string;
  license: string;
}

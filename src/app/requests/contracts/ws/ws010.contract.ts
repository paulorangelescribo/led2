import { OperationFieldsContract } from './../../../contracts/operation-fields.contract';
import { UserAccessFieldsContract } from '../../../contracts/user-access-fields.contract';

export interface Ws010Contract extends OperationFieldsContract, UserAccessFieldsContract {
  device_id: string;
  book_led_id: string;
}

import { UserAccessFieldsContract } from './../../../contracts/user-access-fields.contract';
import { OperationFieldsContract } from '../../../contracts/operation-fields.contract';
export interface Ws013Contract extends OperationFieldsContract, UserAccessFieldsContract {
    book_led_id: string;
    device_id: string;
}

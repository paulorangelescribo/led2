import { UserAccessFieldsContract } from './../../../contracts/user-access-fields.contract';
import { OperationFieldsContract } from '../../../contracts/operation-fields.contract';
export interface Ws015Contract extends OperationFieldsContract, UserAccessFieldsContract {}

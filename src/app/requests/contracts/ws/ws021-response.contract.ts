export interface WS021ResponseContract {
  user_app_token?: string;
  app_token?: string;
  book_publisher_id?: string;
  quantity_array: Array<{ qr_code: string, quantity: number}>;
}

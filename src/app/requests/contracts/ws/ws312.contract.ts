export interface Ws0312Contract {
    operation?: string;
    login: string;
    password: string;
    led_user_id: string;
    device_id: string;
    book_id?: string;
    last_settings_synced: number;
    qr_code?: string;
    settings: any,
    get_updates: number,
    led_app_type: string,
    use_stats: Array<any>,

  }
  
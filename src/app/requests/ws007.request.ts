import { UserLoginFieldsContract } from '../contracts/user-login-fields.contract';
import { Ws007Contract } from './contracts/ws/ws007.contract';
import { DefaultContractRequest } from './contracts/default.contract.request';
import { BookService } from '../modules/services/book.service';
import * as CryptoJS from 'crypto-js';
import { UserLoggedFieldsContract } from '../contracts/user-logged-fields.contract';

export class Ws007Request implements DefaultContractRequest {
  private _service: BookService;
  private _request: Ws007Contract;

  constructor(request?: UserLoginFieldsContract, service?: BookService,
              currentSession?: UserLoggedFieldsContract) {

    this._service = service;
    const DataRequest: any = {};

    if (!request) {
      DataRequest.login = currentSession.login;
      DataRequest.password = currentSession.password;
    }

    this._service.prepareLoginRequest(DataRequest);
    this._service.prepareDeviceInfoRequest();
    this._request = this._service.getLoginRequestParams(true);
    this._request.device_id = currentSession.device_id;
  }

  getHttpRequest(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._service
        .getBookList(this._request)
        .then((httpResult: any) => {
          if (httpResult.error_code !== '' && httpResult.error_code !== '0') {
            return reject(httpResult.error_msg);
          }

          return resolve(httpResult);
        })
        .catch(reject);
    });
  }
}

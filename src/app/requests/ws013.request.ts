import { DefaultContractRequest } from './contracts/default.contract.request';
import { BookService } from '../modules/services/book.service';
import { Ws013Contract } from './contracts/ws/ws013.contract';
import { LogProvider } from '../providers/log.provider';
import { LedFileSystemContract } from '../modules/services/contracts/file-system/led-file-system.contract';

export class Ws013Request implements DefaultContractRequest {
  private Log: LogProvider = LogProvider.getInstance('Ws013Request');

  constructor(public params: Ws013Contract, private bookService: BookService, private fileSystem: LedFileSystemContract) {}

  public getHttpRequest(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getBookFilesManifest()
        .then(resolve)
        .catch(() => {
          this.bookService
            .notifyBookInstallation(this.params)
            .then((response: any) => {
              if (response.error_code !== '' && response.error_code !== '0') return reject(response.error_msg);
              this.saveBookFilesManifest(response, () => resolve(response));
            }).catch(error => {
              this.Log.save(error, 'notifyBookInstallation => getBookFilesManifest');
            });
        });
    });
  }

  getBookFilesManifest(bookLedId?: string): Promise<any> {
    bookLedId = bookLedId ? bookLedId : this.params.book_led_id;
    return new Promise((resolve, reject) => {
      this.fileSystem
          .request(bookLedId + '.files.manifest.json')
          .readFile()
          .toJSON(resolve, reject);
    });
  }

  saveBookFilesManifest(response: any, handler: any, bookLedId?: string) {
    bookLedId = bookLedId ? bookLedId : this.params.book_led_id;
    this.fileSystem
        .request(bookLedId + '.files.manifest.json')
        .writeFile(new Buffer(JSON.stringify(response)),
        handler, error => { console.log(error); });
  }
}

import { DefaultContractRequest } from './contracts/default.contract.request';
import { Ws021Contract } from './contracts/ws/ws021.contract';
import { BookService } from '../modules/services/book.service';
export class Ws021Request implements DefaultContractRequest {

  private _request: Ws021Contract;
  private _service: BookService

  constructor(request: Ws021Contract, service?: BookService) {
      this._request = request;
      this._service = service;
  }

  /**
   * @returns {Promise<any>}
   * @memberof Ws021Request
   */
  getHttpRequest(): Promise<any> {

      return new Promise((resolve, reject) => {
          this._service.requestExternalAnnotations(this._request)
          .then((response: any) => {
              resolve(response.json());
          }).catch(reject);
      });

  }
}
